import numpy as np
import matplotlib.pyplot as plt
import glob
import toml
from time import perf_counter
import json
import random
import shutil
import pickle
import string
import tabulate
import tqdm
import scipy
import scipy.optimize
from scipy.interpolate import UnivariateSpline
from scipy.signal import savgol_filter
import Voronio2
import generate_pickets
import os
import stopit
import subprocess
import matplotlib as mpl
import pandas as pd
from statsmodels.stats.weightstats import DescrStatsW

import pyabc

subprocess.check_output("cargo build --release",shell=True)


baseline = """
[model_param]
particle_radius = 0.4e-9
lattice_distance_force_cutoff = 2.5e-9
temperature = 300.0
k_barr = 0.0e-18
hopp_probability = 0.01
number_of_particles = 1
diffusion = 0.0000000000008
global_drift = [0.0, 0.0]

[geometry_param]
system_size = [1000e-9, 1000e-9]
name_of_input_file = "Pot_mesh_relation_1000L80_2"
input_file_prefactor = 0.000000001
periodic_boundary = [true, true]
buhnen_radius = 0.000000004
barrier_type = "HoppProbability"

[sim_param]
number_of_replications = 1000
timestep = 2000e-9
draw_images = false
output_prefix = "Test_paper_appendex_S4a"
simulation_time = 0.5
observe_every_steps = 100
start_msd_time = 0.01
write_traj = false
"""

#CELL_TYPE = "pyABC_fit/Pickets/P_1_Picket_R2_L80_dr0.8_F1" #"NRK_lab" IA_32" 
CELL_TYPE = "P_1_Picket_R2_L80_dr0.8_F1_hop" #"NRK_lab" #IA_32"


def prepare_config(L,D,k=None,phopp=None,dr=None,reps=1000):
    if k == None and phopp == None and dr == None:
        raise ValueError('No method specified')
    config = toml.loads(baseline)
    random_string = "_"+''.join(random.choice(string.ascii_lowercase) for i in range(25))
    if dr==None:
        dr_samp = 1
    else:
        dr_samp = dr
    @stopit.threading_timeoutable(default='not finished')
    def timeout_generator():
        generate_pickets.save_mesh(R=2, dr=dr_samp, L=L, name=random_string)
    return_val = timeout_generator(timeout=3)
    while return_val == "not finished":
        print("Lattice script failed... retry, L={}, dr={}".format(L,dr))
        return_val=timeout_generator(timeout=4)

    #Voronio2.find_geometry_from_L(L=L,name=random_string)
    if k != None:
        assert(phopp == None)
        assert(dr == None)
        config["geometry_param"]["name_of_input_file"] = "Pot_mesh_{}".format(random_string)
        config["geometry_param"]["barrier_type"] = "PotentialNearest"
        config["model_param"]["k_barr"] = float(np.abs(k*1e-18))
    elif dr != None:
        assert(phopp == None)
        config["geometry_param"]["barrier_type"] = "Buhnen"
        config["geometry_param"]["name_of_input_file"] = "Picket_mesh_{}".format(random_string)
    elif phopp != None:
        config["geometry_param"]["barrier_type"] = "HoppProbability"
        config["geometry_param"]["name_of_input_file"] = "Hop_mesh_{}".format(random_string)
        config["model_param"]["hopp_probability"] = float(phopp)
    else:
        raise ValueError('No method specified')

    config["sim_param"]["output_prefix"] = random_string

    config["model_param"]["diffusion"] = float(np.abs(D))
    config["sim_param"]["number_of_replications"] = int(reps)
    with open(random_string+".toml", "w") as toml_file:
        toml.dump(config, toml_file)
    return random_string



def sample_via_simulation_back(L,D,k=None,phopp=None,dr=None,reps=500,silent=False,print_time=False):
    t_start = perf_counter()
    config = prepare_config(L=L,D=D,k=k,phopp=phopp,dr=dr,reps=reps)
    t_conf = perf_counter()

    if silent:
        #target/release/diffusion_simulation
        subprocess.check_output("target/release/diffusion_simulation -- "+config+".toml",shell=True,stderr=subprocess.STDOUT)
        #subprocess.check_output("cargo run --release -- "+config+".toml",shell=True,stderr=subprocess.STDOUT)
    else:
        subprocess.check_output("target/release/diffusion_simulation -- "+config+".toml",shell=True)
        #subprocess.check_output("cargo run --release -- "+config+".toml",shell=True)

    t_exec = perf_counter()
    filenames_msd = glob.glob("*{}*/msd*.npy".format(config))
    arrays = np.array(list(map(np.load, filenames_msd)))

    folder_name = glob.glob("*_{}_*".format(config))
    #print(folder_name)
    assert(len(folder_name) == 1)
    shutil.rmtree(folder_name[0])
    os.remove(config+".toml")
    os.remove("Hop_mesh_"+config)
    os.remove("Picket_mesh_"+config)
    os.remove("Pot_mesh_"+config)
    t_io = perf_counter()
    time = arrays[0][:, 0][1:]
    msd = np.mean(arrays, axis=0)[:, 1][1:]
    d_x = np.sqrt(4 * D * (time))
    dapp = msd / (4 * time)
    #print(d_x.max())
    s_d_x = np.linspace(d_x.min(),min(d_x.max(),500e-9),num=50*100)
    t_pro1 = perf_counter()
    s_d_app = np.interp(s_d_x,d_x,dapp)
    smooth = savgol_filter(s_d_app,1000+1,3)
    t_pro2 = perf_counter()
    if print_time:
        print("conf:  ",t_conf-t_start)
        print("exec:  ",t_exec-t_conf)
        print("io:    ",t_io-t_exec)
        print("proc1: ",t_pro1-t_io)
        print("proc2: ",t_pro2-t_pro1)
        print("total: ",t_pro2-t_start)
    return (d_x,dapp,s_d_x,smooth)

iter = 0
findings = pd.DataFrame()
best_findings_traj = {}


def get_quality_of_params(L,D,k,reps=50):
    #global findings,iter
    try:
        (x,y,sx,sy) = sample_via_simulation(L=L,D=D,k=k,reps=reps,silent=True)
    except:
        print("fail")
        return np.inf
    #todo change back to _lab
    #ref = pd.read_csv(CELL_TYPE+"_lab.csv")
    ref = pd.read_csv(CELL_TYPE+".csv")
    ref_x = ref["focus"]*1e-9
    ref_y = ref["d_app"]*1e-12
    intp = np.interp(ref_x,sx,sy)
    #print(ref_x)
    #exit()
    #print('ref {} and dat {}'.format(ref_x.mean(),x.mean()))
    #print(intp)
    #print(ref_y)
    mean_factor = np.mean(ref_y)
    #plt.clf()
    #plt.plot(ref_x,ref_y,label="ref")
    #plt.plot(x,y,label="fit")
    #plt.show(block=False)
    #plt.pause(.001)
    #plt.legend()
    quality =  np.mean(np.square(ref_y/mean_factor-intp/mean_factor))

    #print(L,",",k," -> ",quality)
    """plt.clf()
    plt.plot(x*1e9,y*1e12,label="L={:.0f}nm,D={:.2f}μm²/s,P={:.4f} %".format(L,D*1e12,k*100))
    plt.plot(sx*1e9,sy*1e12)
    plt.title("iteration={}, quality={}".format(iter,quality))
    ref = pd.read_csv(CELL_TYPE+".csv")
    ref_x = ref["focus"]
    ref_y = ref["d_app"]
    plt.xlabel("d focus [nm]")
    plt.ylabel("D_apparent [μm²/s]")
    plt.plot(ref_x,ref_y,"-",lw=2,markersize=10,color="black",label="data")
    plt.show()"""
    return quality #TODO remove again

def sample_via_simulation(L,D,k=None,phopp=None,dr=None,reps=500,silent=False,print_time=False,multis = 1):
    #print("Sample {} {} {}".format(L,D,k))
    if multis ==1:
        return sample_via_simulation_back(L,D,k,phopp,dr,reps,silent,print_time)
    xs = []
    ys = []
    c = []
    d = []
    for _ in range(multis):
            (d1,d2,d3,d4) = sample_via_simulation_back(L,D,k,phopp,dr,reps,silent,print_time)
            xs.append(d1)
            ys.append(d2)
            c.append(d3)
            d.append(d4)
    y_mean = np.mean(ys, axis=0)
    s_d_x = np.linspace(xs[0].min(),min(xs[0].max(),500e-9),num=50*100)
    s_d_app = np.interp(s_d_x,xs[0],y_mean)
    smooth = savgol_filter(s_d_app,1000+1,3)
    return (xs[0],y_mean,s_d_x,smooth)


def try_pyabc():
    import os
    import tempfile

    import matplotlib.pyplot as plt
    import numpy as np

    import pyabc
    print(get_quality_of_params(L=10,D=1.004819*1e-12,k=0.014007*1e-18,reps=4))

    def model(parameter): #D=1.004819
        return {"dat": get_quality_of_params(L=parameter["L"],D=parameter["D"]*1e-12,k=parameter["k"],reps=1)}
    prior = pyabc.Distribution(L=pyabc.RV("uniform", 30, 160),k=pyabc.RV("uniform",0,0.05),D=pyabc.RV("uniform",0.3,1.5))
    def distance(x, x0):
        assert(x0["data"]=="Dummy")
        return abs(x["dat"])
    abc = pyabc.ABCSMC(model, prior, distance,population_size=100)#,sampler=pyabc.sampler.SingleCoreSampler())#, population_size=20)#
    db_path = os.path.join(tempfile.gettempdir(), "test.db")
    observation = "Dummy"
    abc.new("sqlite:///" + db_path, {"data": observation})
    history = abc.run(minimum_epsilon=0.001)#, max_nr_populations=10)

def continue_pyabc():
    import os
    import tempfile

    import matplotlib.pyplot as plt
    import numpy as np

    import pyabc
    print(get_quality_of_params(L=10,D=1.004819*1e-12,k=0.014007*1e-18,reps=4))

    def model(parameter): #D=1.004819
        return {"dat": get_quality_of_params(L=parameter["L"],D=parameter["D"]*1e-12,k=parameter["k"],reps=1)}
    prior = pyabc.Distribution(L=pyabc.RV("uniform", 30, 240),k=pyabc.RV("uniform",0,0.05),D=pyabc.RV("uniform",0.3,1.5))
    def distance(x, x0):
        assert(x0["data"]=="Dummy")
        return abs(x["dat"])
    abc = pyabc.ABCSMC(model, prior, distance,population_size=100)#,sampler=pyabc.sampler.SingleCoreSampler())#, population_size=20)#
    db_path = os.path.join(tempfile.gettempdir(), "test2.db")
    observation = "Dummy"
    abc.load("sqlite:///" + db_path)
    history = abc.run(minimum_epsilon=1e-6)#, max_nr_populations=10)

def read_abc():
    import pyabc
    import tempfile
    db_path = os.path.join(tempfile.gettempdir(), "/home/phenning/diffusion_simulation/pyABC_fit/Fit_tri_2.5nm/fitting_k_for_L80_D8e-13_p0.005994842503189409_reps100.db")
    print(db_path)
    history = pyabc.History("sqlite:///" + db_path)
    df,w = history.get_distribution()
    pyabc.visualization.plot_kde_matrix(df,w)
    plt.savefig("pyABC_overview.pdf")
    plt.savefig("pyABC_overview.svg")
    #plt.title(CELL_TYPE)
    #plt.show()
    #return
    #print(df)
    #print(w)
    #x_vals, pdf = pyabc.visualization.kde.kde_1d(df, w, "L")
    #plt.title("L")
    #plt.plot(x_vals,pdf)
    #plt.show()
    #plt.title("k")
    #x_vals, pdf = pyabc.visualization.kde.kde_1d(df, w, "k")
    #plt.plot(x_vals,pdf)
    #plt.show()
    fig, ax = plt.subplots()
    res = pyabc.visualization.plot_credible_intervals(history,arr_ax=ax,par_names=["k"])
    plt.show()
    fig, ax = plt.subplots()
    res = pyabc.visualization.plot_credible_intervals(history,arr_ax=ax,par_names=["L"])
    plt.show()
    fig, ax = plt.subplots()
    res = pyabc.visualization.plot_credible_intervals(history,arr_ax=ax,par_names=["D"])
    plt.show()
    print(res)

def fitting_proc(ref_x_, ref_y_,database_name,method,epsi = 0.001):
    if method=="k":
        def model(parameter):
            (x,y,sx,sy) = sample_via_simulation(L=parameter["L"],D=parameter["D"]*1e-12,k=parameter["k"],reps=1,silent=True)
            return {"x":x, "y":y , "sx":sx,"sy":sy }
        prior = pyabc.Distribution(L=pyabc.RV("uniform", 30, 240),k=pyabc.RV("uniform",0,0.05),D=pyabc.RV("uniform",0.3,1.5))
    elif method == "dr":
        def model(parameter):
            return {"dat": sample_via_simulation(L=parameter["L"],D=parameter["D"]*1e-12,dr=parameter["dr"],reps=1,silent=True)}
        prior = pyabc.Distribution(L=pyabc.RV("uniform", 30, 240),dr=pyabc.RV("uniform",0,20),D=pyabc.RV("uniform",0.3,1.5))
    elif method == "phopp":
        def model(parameter):
            (x,y,sx,sy) = sample_via_simulation(L=parameter["L"],D=parameter["D"]*1e-12,phopp=parameter["phopp"],reps=1,silent=True)
            return {"x":x, "y":y , "sx":sx,"sy":sy }
            #return {"dat": sample_via_simulation(L=parameter["L"],D=parameter["D"]*1e-12,phopp=parameter["phopp"],reps=1,silent=True)} # From Till
        prior = pyabc.Distribution(L=pyabc.RV("uniform", 30, 240),phopp=pyabc.RV("uniform",0,1.),D=pyabc.RV("uniform",0.3,1.5))
    else:
        raise Exception("Invalid method")
    def distance(x, x0):
        ref_x = x0["rx"]
        ref_y = x0["ry"]
        if(np.max(ref_x) > 2e-7):
            ref_x2 = ref_x
            ref_x = np.linspace(np.min(ref_x),2e-7)
            ref_y = np.interp(ref_x,ref_x2,ref_y)
        sx=x["sx"]
        sy=x["sy"]
        intp = np.interp(ref_x,sx,sy)
        mean_factor = np.mean(ref_y)
        quality =  np.mean(np.square(ref_y/mean_factor-intp/mean_factor))
        return quality
    abc = pyabc.ABCSMC(model, prior, distance,population_size=100,sampler=pyabc.sampler.SingleCoreSampler())# MulticoreParticleParallelSampler())#,sampler=pyabc.sampler.SingleCoreSampler())#, population_size=20)#
    try:
        abc.load("sqlite:///" + database_name)
        print("reuse old Database for fitting")
    except ValueError:
        abc.new("sqlite:///" + database_name, {"rx": ref_x_,"ry": ref_y_})#"(ref_x_,ref_y_)})

    min_eps_so_far = abc.history.get_all_populations()["epsilon"].min()

    if (min_eps_so_far < epsi):
        return abc.history
    history = abc.run(minimum_epsilon=epsi)
    print("run done")
    return history

def experiment_fit_to_wetlab():
    db_path = "Wetlab_fit_"+CELL_TYPE+"_hop.db"
    ref = pd.read_csv(CELL_TYPE+".csv")
    ref_x = ref["focus"]*1e-9
    ref_y = ref["d_app"]*1e-12
    fitting_proc(ref_x,ref_y,db_path,method="phopp")

def analysis_fit_to_wetlab():
    db_path = "Wetlab_fit_"+CELL_TYPE+".db"
    history = pyabc.History("sqlite:///" + db_path)

    df,w = history.get_distribution()
    print("D ",np.average(df["D"],weights=w))
    print("k ",np.average(df["phopp"],weights=w))
    #exit()


    df,w = history.get_distribution()
    #pyabc.visualization.plot_kde_matrix(df,w)
    #plt.savefig("pyABC_overview_{}.pdf".format(CELL_TYPE))
    #plt.savefig("pyABC_overview_{}.svg".format(CELL_TYPE))
    #fig, ax = plt.subplots()
    #res = pyabc.visualization.plot_credible_intervals(history,arr_ax=ax,par_names=["k"])
    #plt.savefig("fit_prog_k_{}.pdf".format(CELL_TYPE))
    #plt.savefig("fit_prog_k_{}.svg".format(CELL_TYPE))
    #fig, ax = plt.subplots()
    #res = pyabc.visualization.plot_credible_intervals(history,arr_ax=ax,par_names=["L"])
    #plt.savefig("fit_prog_L_{}.pdf".format(CELL_TYPE))
    #plt.savefig("fit_prog_L_{}.svg".format(CELL_TYPE))
    #fig, ax = plt.subplots()
    #res = pyabc.visualization.plot_credible_intervals(history,arr_ax=ax,par_names=["D"])
    #plt.savefig("fit_prog_D_{}.pdf".format(CELL_TYPE))
    #plt.savefig("fit_prog_D_{}.svg".format(CELL_TYPE))

    df_fin = pd.DataFrame()
    def extract_data(dfh,w,name,dicto):
        D = pyabc.weighted_statistics.weighted_mean(np.array(dfh[name]),w)
        D_range_a = pyabc.weighted_statistics.weighted_quantile(np.array(dfh[name]),weights=w,alpha=0.05)
        D_range_b = pyabc.weighted_statistics.weighted_quantile(np.array(dfh[name]),weights=w,alpha=0.95)
        dicto[name+"_fit"] = D
        dicto[name+"_fit_ci"] = (D_range_a,D_range_b)
        return dicto
    new_dat = {"data_set":CELL_TYPE}
    for n in ["D","phopp","L"]:
        new_dat = extract_data(df,w,n,new_dat)
    df_fin = df_fin.append(new_dat,ignore_index=True)
    df_fin.to_csv("fit_results_"+CELL_TYPE)
    print(df_fin.to_markdown())



def perform_pyabc():
    #Epsilon is the "quality" of our fit. We iterativly improve it
    for epsi in np.logspace(1,-3,num=5):
        df = pd.DataFrame()
        print("Now goal epsilon is ",epsi)
        #We scan 10 different phopp values for the fit
        for p in np.logspace(-4,0,num=10):
            """ GENERATING REFERENCE DATA """
            r_L = 80
            r_D = 0.8e-12
            r_reps = 100
            ref_file_name = "ref_L{}_D{}_php{}_reps_{}.pkl".format(r_L,r_D,p,r_reps)
            try: #see if this exact reference data already exists if so reuse
                with open(ref_file_name, 'rb') as inp:
                    ref_datao = pickle.load(inp)
                print("reuse old reference data")
            except FileNotFoundError:
                print(" -> generate new reference data")
                ref_datao = sample_via_simulation(L=r_L,D=r_D,phopp=p,reps=64,silent=True,multis=r_reps)
                with open(ref_file_name, 'wb') as outp:
                    pickle.dump(ref_datao, outp, pickle.HIGHEST_PROTOCOL)
                with open(ref_file_name, 'rb') as inp:
                    ref_datao = pickle.load(inp)
            (ref_x,ref_y,ref_xs,ref_ys) = ref_datao

            """ PERFORM FITTING """
            h = fitting_proc(ref_xs,ref_ys,"fitting_k_for_L{}_D{}_p{}_reps{}.db".format(r_L,r_D,p,r_reps),method="k",epsi=epsi)

            """ OBSERVATION AND IO """
            dfh,w = h.get_distribution()
            print("p -> ",p)
            #d = DescrStatsW(dfh["D"],weights=w*len(dfh["D"]))
            #fig, ax = plt.subplots()
            #res = pyabc.visualization.plot_credible_intervals(h,arr_ax=ax,par_names=["D"])
            #plt.savefig("AAfit_prog_D_{}.pdf".format(CELL_TYPE))
            def extract_data(dfh,w,name,dicto):
                D = pyabc.weighted_statistics.weighted_mean(np.array(dfh[name]),w)
                D_range_a = pyabc.weighted_statistics.weighted_quantile(np.array(dfh[name]),weights=w,alpha=0.05)
                D_range_b = pyabc.weighted_statistics.weighted_quantile(np.array(dfh[name]),weights=w,alpha=0.95)
                dicto[name+"_fit"] = D
                dicto[name+"_fit_ci"] = (D_range_a,D_range_b)
                return dicto
            new_dat = {"target_p":p}
            for n in ["D","k","L"]:
                new_dat = extract_data(dfh,w,n,new_dat)
            df = df.append(new_dat,ignore_index=True)
            df.to_csv("fit_results")
            print(df.to_markdown())


if __name__ == "__main__":
    #
    #perform_pyabc()
    #experiment_fit_to_wetlab()
    analysis_fit_to_wetlab()
    #fit_only_k()
    #print("Quality of {} reference fit: {}".format(CELL_TYPE,get_quality_of_refernce_fit()))
    #print(get_quality_of_params(L=61.584739,D=1.004819,k=0.014007,reps=32))
    #check_quality()
    #optimize()
    #print(get_quality_of_params(L=100,D=0.8e-12,k=0.5e-18,reps=10))
    #(_,_,a,b) = sample_via_simulation(L=100,D=0.8e-12,phopp=0.1,reps=64,silent=True,print_time=True)
    #(_,_,a1,b1) = sample_via_simulation(L=100,D=0.8e-12,phopp=0.1,reps=64,silent=True,print_time=True,multis=5)
    #plt.plot(a,b,label="one")
    #plt.plot(a1,b1)
    #plt.legend()
    #plt.show()
    #print(prepare_config(L=50,D=0.8e-12,k=0.01e-18,reps=10))
    #generate_data()
    ###plot_data()
    #plot_best()
    #prepare_config(L=50,D=0.8e-12,k=0.6, reps=1)

    #try_pyabc()
    #
    #read_abc()
    #continue_pyabc()
