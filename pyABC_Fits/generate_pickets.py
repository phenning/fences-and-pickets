import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as tmp
import scipy.optimize
from scipy.spatial import Voronoi, voronoi_plot_2d,KDTree
from scipy.optimize import differential_evolution

x_max = 1000
y_max = 1000
N_test = 2

#L_goal = 80
#N = round(x_max*y_max/(L_goal*L_goal))
Diff = []

def is_in_box(point):
    return point[0] > -30 and  point[0] < x_max+30 and point[1] > -30 and  point[1] < y_max+30

def is_intersecting(point1, point2):
    return (not is_in_box(point1) and not is_in_box(point2)) and (is_in_box([point1[0], point2[1]]) or is_in_box([point2[0], point1[1]]))


def find_geometry(N,L,name=""):
    while not True in np.abs(Diff) < L*0.02:
        for T in range(0,N_test):
            x = np.random.uniform(0,x_max,N)
            y = np.random.uniform(0,y_max,N)
            points =  np.array([x,y])
            number = []
            for i in range(-1, 2):
                for j in range(-1, 2):
                    n = np.arange(0,x.size)
                    if i==-1 & j==-1:
                        points =  np.array([x+x_max*i, y+y_max*j])
                        number.append(n)
                    else:
                        points = np.append(points, np.array([x+x_max*i, y+y_max*j]), axis = 1)
                        number.append(n)
            
            points = np.rot90(points)
            vor = Voronoi(points, qhull_options='Qbb Qc Qx')
            
            def PolyArea(x,y):
                return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))
            
            Areas = []
            
            for i in range(0,9*N):
                t = vor.point_region[i]
                if i in range(4*N,5*N):
                    polygon = np.array([vor.vertices[i] for i in vor.regions[t]])
                    Areas.append(PolyArea(polygon[:,0], polygon[:,1]))
            Diff.append(np.mean(np.sqrt(Areas))-L)
            
            if abs(np.mean(np.sqrt(Areas))-L) < L*0.05:
                vec_idx_fildterd = filter(lambda x: x[0] >= 0 and x[1] >=0 ,vor.ridge_vertices)     # removes all lines that end in inf
                vec_coordiants = map(lambda x: [vor.vertices[x[0]],vor.vertices[x[1]]],vec_idx_fildterd)    # all x/y-coordinates that don't end in inf
                vec_coor_fildterd2 = filter(lambda x: is_intersecting(x[0], x[1]),vec_coordiants)    # intersection with the box
                output2 = np.array(list(vec_coor_fildterd2))
                vec_idx_fildterd = filter(lambda x: x[0] >= 0 and x[1] >=0 ,vor.ridge_vertices)     # removes all lines that end in inf
                vec_coordiants = map(lambda x: [vor.vertices[x[0]],vor.vertices[x[1]]],vec_idx_fildterd)    # all x/y-coordinates that don't end in inf
                vec_coor_fildterd = filter(lambda x: is_in_box(x[0]) or is_in_box(x[1]) ,vec_coordiants)    # all lines that end or start in the Box
                output = np.array(list(vec_coor_fildterd))
                output = output.reshape((output.shape[0],4))
                output2 = output2.reshape((output2.shape[0],4))
                if output2.size != 0:
                    output = np.concatenate((output, output2), axis=0)
                np.savetxt('Pot_mesh_{}'.format(name) ,output, delimiter=',')
                number = np.rot90(np.array(number))
                number = np.reshape(number,(9*x.size,1), order='F')
                np.savetxt('Hop_mesh_{}'.format(name), np.append(points, number, axis=1),fmt='%.10e,%.10e,%i,' , delimiter=',')
                #print("Geometry file saved wit L = ", np.mean(np.sqrt(Areas)))
                return output
        if sum(Diff) > 0:
            N = N-1
        else:
            N = N+1

def find_geometry_from_L(L,name=""):
    N = round(x_max*y_max/(L*L))
    return find_geometry(N,L,name)

def get_points_optimizer(R,dr,lines):
    total_line_length = 0
    for l in lines:
        start = np.array([l[0],l[1]])
        end = np.array([l[2],l[3]])
        total_line_length = total_line_length + np.linalg.norm(end-start)

    def gen_points(inp):
        inp = np.clip(inp,0,1)
        assert((inp <= 1.).all())
        assert((inp >= 0.).all())
        inp.sort()
        points = []
        inp = inp * total_line_length
        for l in lines:
            #print(inp)
            start = np.array([l[0],l[1]])
            end = np.array([l[2],l[3]])
            segment_length = np.linalg.norm(end-start)
            inp = inp- segment_length
            for x in inp[(inp < 0)&  (inp > -segment_length)]:
                x = -x
                scale = x/segment_length
                assert(scale >= 0)
                assert(scale <= 1)
                points.append( np.array(start + (end-start)*scale))
        pts= np.array(points)
        #plt.clf()
        #plt.scatter(pts[:,0],pts[:,1])
        #plt.show(block=False)
        #plt.pause(0.001)

        return pts
    def rate_points2(points):
        #if points.shape != (200,2):
        #    print(points)
        #print(points)
        #extra_points = np.concatenate(points,points+[0,1]*y_max,points+[0,-1]*ymax)
        try:
            tree = KDTree(points)
        except:
            return 1e100
        found_dists = []
        for p in points:
            (dists,_) = tree.query(p,k=3)
            found_dists.append(dists[1])
            found_dists.append(dists[2])
        found_dists = np.array(found_dists) - 2*R


        negatives = found_dists[found_dists < 0]
        regulars = found_dists[found_dists > 0]

        rating =  np.abs(regulars - dr).sum() + (np.abs(negatives)*100000).sum()
        #print(rating)
        return rating


    def rate_points(points):
        found_dists = []
        for p in points:
            dists = points - p
            dists = np.linalg.norm(dists,axis=1)
            dists.sort()
            #print(dists)
            found_dists.append(dists[1]-2*R)
            found_dists.append(dists[2]-2*R)
        found_dists = np.array(found_dists)

        negatives = found_dists[found_dists < 0]
        regulars = found_dists[found_dists > 0]

        rating =  np.power(regulars - dr,2).sum() + (np.abs(negatives)*100000).sum()
        print(rating,rate_points2(points))
        return rating

    strategy = "min"
    if strategy== "brute":
        inp = np.random.random((5000,100))
        ratings = list(map(lambda x : rate_points2(gen_points(x)),inp))
        val, idx = min((val, idx) for (idx, val) in enumerate(ratings))
        best_points = gen_points(inp[idx])
        plt.scatter(best_points[:,0],best_points[:,1])
        #best_points = gen_points(inp[0])
        #plt.scatter(best_points[:,0],best_points[:,1])
        plt.show()
    elif strategy == "min":
        bounds = [(0,1)] * 300
        result = scipy.optimize.minimize(lambda x: rate_points2(gen_points(x)),x0=np.linspace(0.01,0.99,num=300),method="Nelder-Mead",bounds=bounds,options={"maxiter":1000,"disp":True})
        print(result)
        best_points = gen_points(result.x)
        plt.scatter(best_points[:,0],best_points[:,1])
        plt.show()
    elif strategy == "gen":
        bounds = [(0,1)] * 150
        result = differential_evolution(lambda x: rate_points2(gen_points(x)),bounds=bounds,disp=True,maxiter=10,popsize=10,polish=False)
        print(result.x.shape)
        result = scipy.optimize.minimize(lambda x: rate_points2(gen_points(x)),x0=result.x,bounds=bounds,options={"maxiter":10,"disp":True})
        #print(result)
        best_points = gen_points(result.x)
        plt.scatter(best_points[:,0],best_points[:,1])
        plt.show()
    else:
        best_points = gen_points(np.linspace(0.01,0.99,num=300))
        plt.scatter(best_points[:,0],best_points[:,1])
        plt.show()

    #print(ratings)
    #print(inp)
    #print(total_line_length)
    #points = gen_points(inp)
    #print(rate_points(points))
    #

def get_points (R, dr, lines):
    Points = []
    for l in range(0, int(np.size(lines)/4)):
        L1 = np.array([lines[l,0], lines[l,1]])
        L2 = np.array([lines[l,2], lines[l,3]])
        d = L2-L1
        X1 = L1 + np.random.uniform(0.25, 0.75)*d
        #X2 = L1 + np.random.uniform(0.25, 0.75)*d
        d = d/np.linalg.norm(d)
        s1 = R*np.array([-d[1], d[0]])
        s1 = s1*0
        p1 = X1 + s1
        #p2 = X2 - s1
        while np.linalg.norm(p1-s1-L1)+R < np.linalg.norm(L2-L1):
            if p1[0]> R and p1[0] < x_max-R and p1[1]>R and p1[1] < y_max-R:
                Points.append(p1)
            elif p1[0]> -R and p1[0] < R and p1[1]>R and p1[1] < y_max-R:
                Points.append(p1)
                Points.append(p1+[x_max,0])
            elif p1[0]> R and p1[0] < x_max-R and p1[1]>-R and p1[1] < R:
                Points.append(p1)
                Points.append(p1+[0, y_max])
            p1 = p1 + d*(2*R+np.random.uniform(0, dr))
        p1 = X1 + s1 - d*(2*R+np.random.uniform(0, dr))    
        while np.linalg.norm(p1-s1-L2)+R < np.linalg.norm(L2-L1):
            if p1[0]> R and p1[0] < x_max-R and p1[1]>R and p1[1] < y_max-R:
                Points.append(p1)
            elif p1[0]> -R and p1[0] < R and p1[1]>R and p1[1] < y_max-R:
                Points.append(p1)
                Points.append(p1+[x_max,0])
            elif p1[0]> R and p1[0] < x_max-R and p1[1]>-R and p1[1] < R:
                Points.append(p1)
                Points.append(p1+[0, y_max])
            p1 = p1 - d*(2*R+np.random.uniform(0, dr))
            

        """while np.linalg.norm(p2+s1-L1)+R < np.linalg.norm(L2-L1):
           if p2[0]> R and p2[0] < x_max-R and p2[1]>R and p2[1] < y_max-R:
                Points.append(p2)
           elif p2[0]> -R and p2[0] < R and p2[1]>R and p2[1] < y_max-R:
                Points.append(p2)
                Points.append(p2+[x_max,0])
           elif p2[0]> R and p2[0] < x_max-R and p2[1]>-R and p2[1] < R:
                Points.append(p2)
                Points.append(p2+[0, y_max])
           p2 = p2 + d*(2*R+np.random.uniform(0, dr))
           
        p2 = X2 - s1 - d*(2*R+np.random.uniform(0, dr))
        while np.linalg.norm(p2+s1-L2)+R < np.linalg.norm(L2-L1):
           if p2[0]> R and p2[0] < x_max-R and p2[1]>R and p2[1] < y_max-R:
                Points.append(p2)
           elif p2[0]> -R and p2[0] < R and p2[1]>R and p2[1] < y_max-R:
                Points.append(p2)
                Points.append(p2+[x_max,0])
           elif p2[0]> R and p2[0] < x_max-R and p2[1]>-R and p2[1] < R:
                Points.append(p2)
                Points.append(p2+[0, y_max])
           p2 = p2 - d*(2*R+np.random.uniform(0, dr))"""
        #print(l)
           
    np.savetxt('buhnen_test',Points, delimiter=',')
    return

def get_points2 (R, dr, lines, name=""):
    Points = []
    for l in range(0, int(np.size(lines)/4)):
        L1 = np.array([lines[l,0], lines[l,1]])
        L2 = np.array([lines[l,2], lines[l,3]])
        d = L2-L1
        L = np.linalg.norm(d)
        d = d/np.linalg.norm(d)
                #p2 = X2 - s1
        if L>2*R and L <4*R+dr:
            p1 = L1 + d*L/2
            Points.append(p1)
        elif L>4*R+dr and L <6*R+dr:
            empty_space = L-4*R
            empty_dist = np.random.uniform(0.0, 1, 3)
            empty_dist = empty_space * empty_dist/sum(empty_dist)
            p1 = L1 + (R + empty_dist[0])*d
            Points.append(p1)
            p1 = p1 + (2*R + empty_dist[1])*d
            Points.append(p1)
        elif L > 6*R+dr:
            N = int(np.floor((L-dr)/(2*R+dr)))
            if N != 0:
                empty_space = L-N*2*R
                empty_dist = np.random.uniform(0.0, 1, N+1)
                empty_dist = empty_space * empty_dist/sum(empty_dist)
                p1 = L1 + (R+empty_dist[0])*d
                Points.append(p1)
                for n in range(1, N):
                    p1 = p1 + (2*R + empty_dist[n])*d
                    Points.append(p1)
                #p1 = L2-R*d
                #Points.append(p1)
        #print(l)
    new_Points = []
    for p in Points:
        if p[0] > -R and p[0] < x_max-R and p[1] > -R and p[1] < y_max-R:
            new_Points.append(p)
        if p[0] > -R and p[0] < R and p[1] > R and p[1] < y_max-R:
            new_Points.append(p + [x_max, 0])
        if p[0] > R and p[0] < x_max-R and p[1] > -R and p[1] < R:
            new_Points.append(p + [0, y_max])
    np.savetxt('Picket_mesh_{}'.format(name),new_Points, delimiter=',')
    return

def save_mesh(R, dr, L, name):
    lines = find_geometry_from_L(L=L, name=name)
    get_points2(R=R, dr=dr, lines=lines, name=name)
    return

if __name__ == "__main__":
    save_mesh(R=2, dr=2, L=80, name="str_test")

    #find_geometry(N)
    #lines = find_geometry_from_L(50)
    #get_points2(2, 2, lines)
    #Points = get_points_optimizer(4,0.2,lines)

"""fig = voronoi_plot_2d(vor)
ax = plt.gca()
for i in range(0,9*N):
    t = vor.point_region[i]
    if i in range(4*N,5*N):
        polygon = [vor.vertices[i] for i in vor.regions[t]]
        plt.fill(*zip(*polygon))
        plt.plot(vor.points[i,0],vor.points[i,1], 'ro')
        rect = tmp.Rectangle((0, 0), 1500, 1500,linewidth=1,edgecolor='r',facecolor='none')
        ax.add_patch(rect)
plt.show()



vec_idx_fildterd = filter(lambda x: x[0] >= 0 and x[1] >=0 ,vor.ridge_vertices)
vec_coordiants = map(lambda x: [vor.vertices[x[0]],vor.vertices[x[1]]],vec_idx_fildterd)
vec_coor_fildterd = filter(lambda x: is_in_box(x[0]) or is_in_box(x[1]) ,vec_coordiants)
    
output = np.array(list(vec_coor_fildterd))
output = output.reshape((output.shape[0],4))
np.savetxt('geometry',output, delimiter=',')
"""








