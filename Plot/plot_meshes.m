clear all;
clc;

dx = 100;
dy = 100;


files = '..\Models\Picket_mesh_L_50_small_R4_dr0.8';
pickets = load(files);

files = '..\Models\Pot_mesh_L_50_small';
lines = load(files);

files = '..\Models\Hop_mesh_L_50_small';
points = load(files);

files = '..\Models\P_1_pot_L50_small_quiver';
force_x = readNPY([files,'\force_x.npy']);
force_y = readNPY([files,'\force_y.npy']);
[X,Y] = meshgrid(1:2:99,1:2:99);

figure
subplot(1,3,3)
R = 4;
phi = linspace(0, 2*pi, 50);
hold on
for i=1:numel(pickets(:,1))
    x = pickets(i,1)+ R*sin(phi);
    y = pickets(i,2)+ R*cos(phi);
    plot(x,y, 'k')
%     plot(buhen(i,1), buhen(i,2), 'r.')
end
xlabel('x [nm]')
ylabel('y [nm]')
axis equal
xlim([0 dx])
ylim([0 dy])
box on;
title('Picket fences')
subplot(1,3,2)
quiver(X, Y, force_x', force_y', 'k')
xlabel('x [nm]')
ylabel('y [nm]')
axis equal
xlim([0 dx])
ylim([0 dy])
title('Potential fences')

subplot(1,3,1)
for i=1:numel(lines(:,1))
    line([lines(i,1) lines(i,3)], [lines(i,2) lines(i,4)],...
    'Color','black', 'LineWidth', 1)
end
xlabel('x [nm]')
ylabel('y [nm]')
axis equal
xlim([0 dx])
ylim([0 dy])
box on;
title('Probability fences')
set(findall(gcf,'-property','FontSize'),'FontSize',16)