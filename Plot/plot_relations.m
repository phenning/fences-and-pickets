clear all;
clc;

%load data

filename = '..\data_for_plots\pyABC_fits\Fit_tri_5nm\fit_results';
B = readtable(filename);

target_p = B{:,2};
D_fit = B{:,3};
D_fit_ci = B{:,4};
k_fit = B{:,5};
k_fit_ci = B{:,6};
L_fit = B{:,7};
L_fit_ci = B{:,8};


D_fit_ci = erase(D_fit_ci, ["(", ")", ',']);
D_fit_ci = split(D_fit_ci);
D_fit_ci = str2double(D_fit_ci);

k_fit_ci = erase(k_fit_ci, ["(", ")", ',']);
k_fit_ci = split(k_fit_ci);
k_fit_ci = str2double(k_fit_ci);

L_fit_ci = erase(L_fit_ci, ["(", ")", ',']);
L_fit_ci = split(L_fit_ci);
L_fit_ci = str2double(L_fit_ci);

%plot relation P_hop -> k_fence

figure(5565)
H = subplot(3,1,1);
errorbar(target_p*100, k_fit, k_fit_ci(:,1)-k_fit, k_fit_ci(:,2)-k_fit,'LineWidth',2)
H.XScale='log';
hold on
% xlabel('P [%]')
ylabel('k [aJ]')

H = subplot(3,1,2);
errorbar(target_p*100, L_fit, L_fit_ci(:,1)-L_fit, L_fit_ci(:,2)-L_fit,'LineWidth',2)
hold on;
line([0.01 100], [80 80], 'Color','black','LineWidth',2, 'LineStyle', '--')
H.XScale='log';
hold on
% xlabel('P [%]')
ylabel('L [nm]')

H = subplot(3,1,3);
errorbar(target_p*100, D_fit, D_fit_ci(:,1)-D_fit, D_fit_ci(:,2)-D_fit,'LineWidth',2)
line([0.01 100], [0.8 0.8], 'Color','black','LineWidth',2, 'LineStyle', '--')
H.XScale='log';
hold on
xlabel('P [%]')
ylabel('D_0 [µm²/s]')
set(findall(gcf,'-property','FontSize'),'FontSize',16)

%% adds picket data to the relation

clear all;
clc;
filename = '..\data_for_plots\pyABC_fits\Pickets\fit_results_P_1_Picket_R4_L80_dr0-8_F1';
B = readtable(filename+"_hop");

target_p = B{:,2};
D_fit_hop = B{:,3};
D_fit_ci_hop = B{:,4};
P_fit = B{:,5};
P_fit_ci = B{:,6};
L_fit_hop = B{:,7};
L_fit_ci_hop = B{:,8};

D_fit_ci_hop = erase(D_fit_ci_hop, ["(", ")", ',']);
D_fit_ci_hop = split(D_fit_ci_hop);
D_fit_ci_hop = str2double(D_fit_ci_hop);

P_fit_ci = erase(P_fit_ci, ["(", ")", ',']);
P_fit_ci = split(P_fit_ci);
P_fit_ci = str2double(P_fit_ci);

L_fit_ci_hop = erase(L_fit_ci_hop, ["(", ")", ',']);
L_fit_ci_hop = split(L_fit_ci_hop);
L_fit_ci_hop = str2double(L_fit_ci_hop);

B = readtable(filename+"_pot");

target_p = B{:,2};
D_fit_pot = B{:,3};
D_fit_ci_pot = B{:,4};
k_fit = B{:,5};
k_fit_ci = B{:,6};
L_fit_pot = B{:,7};
L_fit_ci_pot = B{:,8};

D_fit_ci_pot = erase(D_fit_ci_pot, ["(", ")", ',']);
D_fit_ci_pot = split(D_fit_ci_pot);
D_fit_ci_pot = str2double(D_fit_ci_pot);

k_fit_ci = erase(k_fit_ci, ["(", ")", ',']);
k_fit_ci = split(k_fit_ci);
k_fit_ci = str2double(k_fit_ci);

L_fit_ci_pot = erase(L_fit_ci_pot, ["(", ")", ',']);
L_fit_ci_pot = split(L_fit_ci_pot);
L_fit_ci_pot = str2double(L_fit_ci_pot);



figure(5565)
H = subplot(3,1,1);
errorbar(P_fit*100, k_fit, k_fit_ci(1)-k_fit, k_fit_ci(2)-k_fit, (P_fit_ci(1)-P_fit)*100, (P_fit_ci(2)-P_fit)*100, 'LineWidth',2, 'Marker', 'o', 'LineStyle', 'none')
H.XScale='log';
hold on
% xlabel('P [%]')
ylabel('k [aJ]')

