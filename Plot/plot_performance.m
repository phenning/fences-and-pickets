clear all;
clc;

fname = '..\data_for_plots\Performance\perf_plot_data.json'; 
fid = fopen(fname); 
raw = fread(fid,inf); 
str = char(raw'); 
fclose(fid); 
val = jsondecode(str);


figure(159)
p1 = semilogx(val.Picket.small(1,:), val.Picket.small(2,:), 'color', '#0072BD', 'linestyle', '--', 'Linewidth', 2);
hold on
p2 = semilogx(val.Picket.large(1,:), val.Picket.large(2,:), 'color', '#0072BD', 'linestyle', '-', 'Linewidth', 2);

p3 = semilogx(val.Potential.small(1,:), val.Potential.small(2,:), 'color', '#D95319', 'linestyle', '--', 'Linewidth', 2);
p4 = semilogx(val.Potential.large(1,:), val.Potential.large(2,:), 'color', '#D95319', 'linestyle', '-', 'Linewidth', 2);

p5 = semilogx(val.Hop.small(1,:), val.Hop.small(2,:), 'color', '#EDB120', 'linestyle', '--', 'Linewidth', 2);
p6 = semilogx(val.Hop.large(1,:), val.Hop.large(2,:), 'color', '#EDB120', 'linestyle', '-', 'Linewidth', 2);

p7 = semilogx(val.Free.small(1,:), val.Free.small(2,:), 'color', '#7E2F8E', 'linestyle', '--', 'Linewidth', 2);
p8 = semilogx(val.Free.large(1,:), val.Free.large(2,:), 'color', '#7E2F8E', 'linestyle', '-', 'Linewidth', 2);

d1 = semilogx(1,0, 'color', '#979797', 'linestyle', '-', 'Linewidth', 2);
d2 = semilogx(1, 0, 'color', '#979797', 'linestyle', '--', 'Linewidth', 2);

legend([p6 p2 d1 p4 p8 d2],'Probabilistic', 'Picket', 'large sys.', 'Potetnial', 'free', 'small sys.')
xlabel('number if particles')
ylabel('steps per second per particle')
set(findall(gcf,'-property','FontSize'),'FontSize',24)






















