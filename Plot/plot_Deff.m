clear all;
clc;

% loads data
path = '../data_for_plots\N_1_timestep\P_1_picket_L50_R4_dr0.8_dt2_small';
files = dir(path);
j=1;
length_load = 0;
i = 1;
while length_load == 0
    if (contains(files(i).name,  'msd'))
        path = [files(i).folder, '\', files(i).name];
        data = readNPY(path);
        length_load = numel(data(:,2));
        i=i+1;
    end
    i=i+1;
end
MSD = zeros(length_load,1);
for i=1:numel(files)
    if contains(files(i).name,  'msd')
        path = [files(i).folder, '\', files(i).name];
        data = readNPY(path);
        dist(:,j) = sqrt(data(:,2))*1e9;
        MSD(:) = MSD + data(:,2);
        j=j+1;
    end
end
MSD = MSD/(j-1)*1e18;           % [nm²]
time = data(:,1);               % [s]
dt = (data(3,1) - data(2,1));   % [s]
D_eff = 1e-6*MSD./(4*time);     % [µm²/s]

%plot data
figure(200)
subplot(1,2,1)
plot(time, MSD, 'linewidth', 2)
hold on
xlim([0 0.007])
% ylim([0 1.5e4])
xlabel('t [s]')
ylabel('MSD [nm²]')
ax = gca;
ax.YAxis.Exponent = 4;
title('Hop')

subplot(1,2,2)
plot(time, D_eff, 'linewidth', 2)
xlim([0 0.007])
% ylim([0.45 0.75])
hold on
xlabel('t [s]')
ylabel('D_{eff} [µm²/s]')
set(findall(gcf,'-property','FontSize'),'FontSize',36)

