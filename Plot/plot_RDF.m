clear all;
clc;

files = dir('..\data_for_plots\N_many_drift\P_500_Picket_L50_dt2_drift_v0_2');
j = 1;
dx = 100;
dy = 500;
for i=1:numel(files)
    if contains(files(i).name,  'trajectory_r1')
        path = [files(i).folder, '\', files(i).name];
        data = readNPY(path);
        y_data(:,j) = data(1:end,3);     % [nm]
        x_data(:,j) = data(1:end,2);     % [nm]
        j = j+1;
    end
end
y_data = [y_data, y_data, y_data];
x_data = [x_data, x_data+dx, x_data-dx];


time = data(1:end,1);       % [ns]
time = time-time(1);
dt = time(2)-time(1);       % [ns]

tic

time = 550:2550;
edges = [0:0.1:4.4, 4.5:0.5:20];
s = 0;

l=0;
for i=1:500
    for t = time
        x = x_data(t,i);
        y = y_data(t,i);
        if (y>400 & y<500)
            r = sqrt((x-x_data(t,:)).^2 + (y-y_data(t,:)).^2);
            h_up = (edges+y)-dy;
            h_up = h_up.*(h_up>0);
            A_up = edges.^2.*acos(1-h_up./edges)-(edges-h_up).*sqrt(2*edges.*h_up+h_up.^2);
            A_up(1)=0;
            h_down = edges-y;
            h_down = h_down.*(h_down>0);
            A_down = edges.^2.*acos(1-h_down./edges)-(edges-h_down).*sqrt(2*edges.*h_down+h_down.^2);
            A_down(1)=0;          
            v = (pi*edges(2:end).^2 - A_up(2:end) - A_down(2:end)) - (pi*edges(1:end-1).^2 - A_up(1:end-1) - A_down(1:end-1));
            [counts, centers] = histcounts(r,edges);
            counts(1) = counts(1)-numel(t);
            s = s+counts./v;
            l = l+1;
        end
    end
    fprintf('%i/%i done. \n', i, numel(x_data(1,:))/3);
end
rho = numel(x_data(1,:))/3/(dx*dy);

figure(125)
plot((edges(2:end)+edges(1:end-1))/2, s/l/rho, 'linewidth', 2)
hold on
xlabel('distance [nm]')
ylabel('g(dist)')

toc

