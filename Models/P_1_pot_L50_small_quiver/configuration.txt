##2022-03-18T20.03.05+01.00__P_1_pot_L50_small_quiver_hhJP##
ModelSpecification {
    model_param: ReadInModelParameters {
        particle_radius: 0.0000000004 m^1,
        lattice_distance_force_cutoff: 0.0000000025 m^1,
        temperature: 300.0 K^1,
        field_frequency: None,
        k_barr: 0.0000000000000000000132 m^2 kg^1 s^-2,
        hopp_probability: Some(
            0.09,
        ),
        number_of_particles: 1,
        diffusion: 0.0000000000008 m^2 s^-1,
        global_drift: [
            0.0 m^1 s^-1,
            0.0 m^1 s^-1,
        ],
    },
    geometry_param: ReadInGeometryParameters {
        system_size: [
            0.0000001 m^1,
            0.0000001 m^1,
        ],
        name_of_input_file: "Pot_mesh_L_50_small",
        input_file_prefactor: 0.000000001 m^1,
        periodic_boundary: [
            true,
            true,
        ],
        buhnen_radius: Some(
            0.000000004 m^1,
        ),
        barrier_type: PotentialNearest,
    },
    sim_param: ReadInSimulationParameters {
        number_of_replications: 1,
        timestep: 0.00002 s^1,
        draw_images: false,
        output_prefix: "P_1_pot_L50_small_quiver",
        simulation_time: 0.41 s^1,
        observe_every_steps: 1,
        start_msd_time: Some(
            0.01 s^1,
        ),
        write_traj: Some(
            false,
        ),
    },
    total_friction: 0.00000000517743375 kg^1 s^-1,
}