# fences and pickets

## Getting started
This repository contains the simulation software used in the publication "Implications of different membrane compartmentalization
models in particle-based in-silico studies", the Matlab scrips for plotting, and the original data of the paper.

To run a model use `cargo run --release -- Model.toml`.
All models used in the publication can be found in the "Models" folder.

The data used to generate the paper's figures are provided in the "data_for_plots" folder. All Matlab scripts necessary to create the figure are in the folder "Plot".

"benches" contains a function to measure the runtime to the simulator and "pyABC_Fits" the python scripts to generate Voronoi mesh or a new model file and a python script to perform a pyABC fit.
