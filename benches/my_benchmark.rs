use criterion::measurement::WallTime;
use criterion::*;
use diffusion_sim::*;
use statrs::generate;
use std::collections::BTreeSet;
use std::fs::File;
use std::io::Read;

fn bench_config_range(group: &mut BenchmarkGroup<WallTime>, mut ms: ModelSpecification) {
    let sizes: BTreeSet<_> = generate::log_spaced(100, 1.0_f64.log10(), 4.)
        .iter().chain(generate::log_spaced(6,4.,5.).iter())
        .map(|k| *k as usize)
        .collect();

    for size in sizes {
        group.throughput(criterion::Throughput::Elements(size as u64));
        ms.model_param.number_of_particles = size as usize;
        let mut sim = SimulationSystem::new_random(ms.clone());
        group.bench_with_input(BenchmarkId::from_parameter(size), &size, |b, &_size| {
            b.iter(|| sim.step(ms.sim_param.timestep.value));
        });
    }
}

fn bench_a(c: &mut Criterion) {
    for method in [
        BarrierType::PotentialNearest,
        BarrierType::NoBarrier,
        BarrierType::HoppProbability,
        BarrierType::Buhnen,
    ]
    .iter()
    {
        for size_name in ["small", "large"].iter() {
            let name = format!("Test_files/scenario_{}.toml", size_name);
            let mut content: String = "".to_string();
            File::open(name.clone())
                .expect("file not found")
                .read_to_string(&mut content)
                .expect("reading file failed");
            let ipf = toml::from_str(&content).expect("could not parse config file");
            let mut ms = ModelSpecification::generate_from_input_file(ipf);
            ms.geometry_param.barrier_type = method.clone();
            let method_str = match method {
                BarrierType::Buhnen => {
                    ms.geometry_param.name_of_input_file = ms
                        .geometry_param
                        .name_of_input_file
                        .replace("Pot_mesh", "Picket_mesh");
                    "Picket"
                }
                BarrierType::PotentialSum => {
                    unimplemented!()
                }
                BarrierType::PotentialNearest => "Potential",
                BarrierType::HoppProbability => {
                    ms.geometry_param.name_of_input_file = ms
                        .geometry_param
                        .name_of_input_file
                        .replace("Pot_mesh", "Hop_mesh");
                    "Hop"
                }
                BarrierType::NoBarrier => "Free",
            };
            let plot_config = PlotConfiguration::default().summary_scale(AxisScale::Logarithmic);
            let mut group = c.benchmark_group(format!("{}/{}", method_str, size_name));
            group.plot_config(plot_config);
            bench_config_range(&mut group, ms);
        }
    }
}

fn criterion_benchmark(c: &mut Criterion) {
    bench_a(c);
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
