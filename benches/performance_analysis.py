import numpy as np
import matplotlib.pyplot as plt
import subprocess
import toml
import time
import glob
import json


def runtime_from_toml(tml):
    test_duration = 15.
    subprocess.check_output("cargo build --release ", shell=True)
    v_time = tml["sim_param"]["simulation_time"]
    start = time.time()
    while float(time.time() - start) < test_duration:
        tml["sim_param"]["simulation_time"] = v_time
        tml["sim_param"]["start_msd_time"] = v_time * 2
        v_time *= 2
        with open("test_file.toml", "w") as toml_file:
            toml.dump(tml, toml_file)
        start = time.time()
        try:
            outpt = subprocess.check_output("cargo run --release -- test_file.toml", shell=True,
                                            timeout=test_duration * 10 + 5).decode("utf-8")
        except subprocess.TimeoutExpired:
            return None

        out = outpt.split(" steps per second)")[0].split("s (")[-1]
        print(time.time() - start, " for ", v_time, float(time.time() - start) < 1.)
    tml["sim_param"]["simulation_time"] = v_time * test_duration / (time.time() - start)
    return float(out)


def name_lookup(x):
    if x == "Buhnen":
        return "Picket"
    if x == "PotentialNearest":
        return "Potential"
    if x == "NoBarrier":
        return "free"
    if x == "HoppProbability":
        return "Hopp"
    panic


def get_particle_number_scan(tml):
    num_ptcl = np.unique(np.logspace(0, 4, num=64, dtype=int))
    free_system = []
    last_n = 1
    for n in num_ptcl:
        tml["model_param"]["number_of_particles"] = int(n)
        tml["sim_param"]["simulation_time"] = float(tml["sim_param"]["simulation_time"] * (last_n / n) * 0.7)
        last_n = n
        res = runtime_from_toml(tml)
        if res != None:
            free_system.append(runtime_from_toml(tml) * n)
        else:
            free_system.append(None)
    return (num_ptcl, free_system)


def gen_data():
    data = {}
    for size in ["small", "large"]:
        data[size] = {}
        for (k, barrier_type) in enumerate(["Buhnen", "PotentialNearest", "HoppProbability", "NoBarrier"]):
            tml = toml.load("Test_files/scenario_" + size + ".toml")
            if barrier_type == "Buhnen":
                tml["geometry_param"]["name_of_input_file"] = tml["geometry_param"]["name_of_input_file"].replace(
                    "Pot_mesh", "Picket_mesh")
            if barrier_type == "HoppProbability":
                tml["geometry_param"]["name_of_input_file"] = tml["geometry_param"]["name_of_input_file"].replace(
                    "Pot_mesh", "Hop_mesh")
            print(barrier_type, tml["geometry_param"]["name_of_input_file"])
            tml["geometry_param"]["barrier_type"] = barrier_type
            (num, dat) = get_particle_number_scan(tml)
            print(num)
            print(dat, barrier_type)
            data[size][barrier_type] = (num, dat)
            print(data)
            with open("perf_cache.toml", "w") as toml_file:
                toml.dump(data, toml_file)


def plot_data():
    data = toml.load("perf_cache.toml")
    for (k, barrier_type) in enumerate(["Buhnen", "PotentialNearest",
                                        "HoppProbability",
                                        "NoBarrier"]):
        (num, small) = data["small"][barrier_type]
        num = np.array(num, dtype=np.float64)
        small = np.array(small, dtype=np.float64)
        plt.semilogx(num, small, ":", color="C{}".format(k))  # label=name_lookup(barrier_type)+" small",

    for (k, barrier_type) in enumerate(["Buhnen", "PotentialNearest",
                                        "HoppProbability",
                                        "NoBarrier"]):
        (num, small) = data["large"][barrier_type]
        if k == 1:
            plt.semilogx([], [], label="large sys", color="grey")
        if k == 2:
            plt.semilogx([], [], ":", label="small sys", color="grey")
        num = np.array(num, dtype=np.float64)
        small = np.array(small, dtype=np.float64)

        plt.semilogx(num, small, label=name_lookup(barrier_type), color="C{}".format(k))
    plt.gcf().set_size_inches(6, 3.5)
    plt.tight_layout()
    plt.ylim(ymax=1.85e7)
    plt.legend(ncol=4)
    plt.xlabel("number of particles")
    plt.ylabel("steps per second per particle")
    plt.savefig("runtime.pdf", bbox_inches="tight")


def data_from_bench():
    fino = {}
    for (k, method) in enumerate(["Picket", "Potential", "Hop", "Free"]):
        fino[method] = {}
        for size in ["small", "large"]:

            runs = glob.glob("target/criterion/" + method + "_" + size + "/*")
            x = []
            y = []
            #print(runs)
            for r in runs:
                if not "report" in r:
                    i = int(r.split("/")[-1])

                    dat = json.load(
                        open("target/criterion/" + method + "_" + size + "/" + str(i) + "/new/estimates.json"))
                    dat = float(dat["mean"]["point_estimate"]) * 1e-9
                    # print(i,dat)
                    x.append(i)
                    y.append(i / dat)
            if (len(x) > 10):
                x.append(15.5)
                y.append(float("nan"))
            sort_idx = np.argsort(x)
            x = np.array(x)[sort_idx]
            y = np.array(y)[sort_idx]
            fino[method][size] = [x.tolist(),y.tolist()]
            # print(x,y)

            if size == "small":
                if k == 1:
                    plt.semilogx([], [], label="large sys", color="grey")
                if k == 2:
                    plt.semilogx([], [], ":", label="small sys", color="grey")

                plt.semilogx(x, y, ":", label=method, color="C{}".format(k))
            else:
                plt.semilogx(x, y, color="C{}".format(k))
    with open('perf_plot_data.json', 'w') as f:
        json.dump(fino,f,indent=2)
    plt.gcf().set_size_inches(6, 3.5)
    plt.tight_layout()
    plt.legend(ncol=4)
    plt.xlabel("number of particles")
    plt.ylim(ymax=1.95e7)
    plt.ylabel("steps per second per particle")
    plt.savefig("runtime.pdf", bbox_inches="tight")


data_from_bench()
# gen_data()
# plot_data()
