mod model_functions;

use image::{Rgb, RgbImage};

use ::core::mem::swap;
use imageproc::drawing::{draw_filled_circle_mut, draw_filled_rect_mut, draw_line_segment_mut};
use imageproc::rect::Rect;
use itertools::izip;
use itertools::Itertools;
use nalgebra::*;
use rand::prelude::*;
use rand_distr::StandardNormal;
use rand_xoshiro::rand_core::SeedableRng;
use rand_xoshiro::Xoshiro256PlusPlus;
use serde::Serialize;
use std::fs::File;
use std::time::Instant;

pub use model_functions::*;
//use indicatif::{ProgressBar, ProgressStyle};
use indicatif::{ProgressBar, ProgressStyle};
use ndarray::Array2;
use ndarray_npy::write_npy;
use std::collections::HashSet;
use std::marker::PhantomData;
use uom::si;
use uom::si::f64::*;
use uom::si::Quantity;

const LOOKUP_N: usize = 50;
const SMALL_PARTICLE_LIMIT: usize = 16;

fn transpose2<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>> {
    assert!(!v.is_empty());
    let len = v[0].len();
    let mut iters: Vec<_> = v.into_iter().map(|n| n.into_iter()).collect();
    (0..len)
        .map(|_| {
            iters
                .iter_mut()
                .map(|n| n.next().unwrap())
                .collect::<Vec<T>>()
        })
        .collect()
}

fn generate_random_normalized_vector(rng: &mut Xoshiro256PlusPlus) -> Vector2<f64> {
    // very slow becaus of angle functions
    /*let random_angle = thread_rng().gen_range(0.0..2. * std::f64::consts::PI);
    vector![random_angle.cos(), random_angle.sin()]*/
    // let ret = vector![rng.sample(StandardNormal), rng.sample(StandardNormal)].normalize(); // Philipp: I don't know why you are normalizing the vector. It is not needed
    let ret = vector![rng.sample(StandardNormal), rng.sample(StandardNormal)];
    //assert_eq!(ret.norm(), 1.);
    ret
}

#[derive(Clone, Debug)]
struct Particle {
    position: Point2<f64>,
}
impl Particle {
    fn overlapps_fast(&self, other: &Particle, radius: f64) -> bool {
        (&self.position - &other.position).norm_squared() < 2. * radius * 2. * radius
    }
    fn distance(&self, other: &Particle, radius: f64) -> f64 {
        (&self.position - &other.position).norm() - 2. * radius
    }
}

#[derive(Debug)]
struct SingleBuhne {
    position: Point2<f64>,
}
impl SingleBuhne {
    fn overlapps_fast(&self, other: &Particle, particle_radius: f64, buhn_radius: f64) -> bool {
        (&self.position - &other.position).norm_squared() < (buhn_radius + particle_radius).powi(2)
    }
    fn distance(&self, other: &Particle, particle_radius: f64, buhn_radius: f64) -> f64 {
        (&self.position - &other.position).norm() - buhn_radius - particle_radius
    }
    fn buhne_force(
        &self,
        particle: &Particle,
        config: &ModelSpecification,
    ) -> Option<Vector2<f64>> {
        let particle_radius: f64 = config.model_param.particle_radius.value;
        let buhn_radius = config
            .geometry_param
            .buhnen_radius
            .expect("No buhnen radius specified!")
            .value;
        if !self.overlapps_fast(particle, particle_radius, buhn_radius) {
            return None;
        }
        let d = self.distance(particle, particle_radius, buhn_radius);
        Some(
            config
                .force_magnitude_between_particles(Quantity {
                    dimension: PhantomData,
                    units: PhantomData,
                    value: -d,
                })
                .value
                * (&particle.position - &self.position).normalize(),
        )
    }
    fn generate_4_endpointns_of_influence(&self, config: &ModelSpecification) -> [Point2<f64>; 4] {
        let buhn_radius = config
            .geometry_param
            .buhnen_radius
            .expect("No buhnen radius specified!")
            .value;
        //panic!("check oder of deltas");
        [
            &self.position + vector![1., 1.] * buhn_radius,
            &self.position + vector![1., -1.] * buhn_radius,
            &self.position + vector![-1., 1.] * buhn_radius,
            &self.position + vector![-1., -1.] * buhn_radius,
        ]
    }
    fn min_extension(&self, config: &ModelSpecification) -> Point2<f64> {
        let ext = self.generate_4_endpointns_of_influence(config);
        point![
            ext.iter()
                .map(|k| k[0])
                .fold(f64::INFINITY, |a, b| a.min(b)),
            ext.iter()
                .map(|k| k[1])
                .fold(f64::INFINITY, |a, b| a.min(b))
        ]
    }
    fn max_extension(&self, config: &ModelSpecification) -> Point2<f64> {
        let ext = self.generate_4_endpointns_of_influence(config);
        point![
            ext.iter()
                .map(|k| k[0])
                .fold(f64::NEG_INFINITY, |a, b| a.max(b)),
            ext.iter()
                .map(|k| k[1])
                .fold(f64::NEG_INFINITY, |a, b| a.max(b))
        ]
    }
}

#[derive(Debug)]
struct AreaCenter {
    position: Point2<f64>,
    id: usize,
}
impl AreaCenter {
    fn distance_sqrt(&self, other: &Particle) -> f64 {
        (&self.position - &other.position).norm_squared()
    }

    fn distance(&self, other: &Particle) -> f64 {
        (&self.position - &other.position).norm()
    }
}

#[derive(Debug)]
struct LatticeElement {
    start_point: Point2<f64>,
    end_point: Point2<f64>,
}
impl LatticeElement {
    fn lattice_force_and_distance(
        &self,
        particle: &Particle,
        config: &ModelSpecification,
    ) -> Option<(Vector2<f64>, f64)> {
        // shift vectors such that lattice element starts in origin
        let lattice_relative = self.end_point.clone() - &self.start_point;
        let particle_relative = particle.position.clone() - &self.start_point;
        let lattice_relative_norm = lattice_relative.normalize();
        let projected = particle_relative.dot(&lattice_relative_norm);
        //println!("{} & {} -> {}", relative, relative_position, projected);
        if projected < 0. || projected * projected > lattice_relative.norm_squared() {
            None
        } else {
            // println!("add forec!");
            let rotated_relative_norm =
                vector![-lattice_relative_norm[1], lattice_relative_norm[0]];
            //let rotated_relative_norm = rotated_relative.normalized();
            let distance = rotated_relative_norm.dot(&particle_relative);

            if distance.abs() >= config.model_param.lattice_distance_force_cutoff.value {
                return None;
            } else {
                //println!("{}", distance * 1e9);
                let magnitude = config
                    .force_magnitude_of_lattice_on_particle(Quantity {
                        dimension: PhantomData,
                        units: PhantomData,
                        value: distance.abs(),
                    })
                    .value;

                Some((
                    distance.signum() * magnitude * rotated_relative_norm,
                    distance.abs(),
                ))
            }
        }
    }
    fn generate_4_endpointns_of_influence(&self, config: &ModelSpecification) -> [Point2<f64>; 4] {
        let distance = config.model_param.lattice_distance_force_cutoff.value;
        let relative = &self.end_point - &self.start_point;
        let relative_rot_norm = (vector![-relative[1], relative[0]]).normalize() * distance;
        [
            self.start_point + relative_rot_norm,
            self.start_point - relative_rot_norm,
            self.end_point + relative_rot_norm,
            self.end_point - relative_rot_norm,
        ]
    }
    fn min_extension(&self, config: &ModelSpecification) -> Point2<f64> {
        let ext = self.generate_4_endpointns_of_influence(config);
        point![
            ext.iter()
                .map(|k| k[0])
                .fold(f64::INFINITY, |a, b| a.min(b)),
            ext.iter()
                .map(|k| k[1])
                .fold(f64::INFINITY, |a, b| a.min(b))
        ]
    }
    fn max_extension(&self, config: &ModelSpecification) -> Point2<f64> {
        let ext = self.generate_4_endpointns_of_influence(config);
        point![
            ext.iter()
                .map(|k| k[0])
                .fold(f64::NEG_INFINITY, |a, b| a.max(b)),
            ext.iter()
                .map(|k| k[1])
                .fold(f64::NEG_INFINITY, |a, b| a.max(b))
        ]
    }
}

#[derive(Serialize)]
struct OutputRow {
    time: f32,
    x: f32,
    y: f32,
}

pub struct SimulationSystem {
    particles: Vec<Particle>,
    particle_associated_area: Vec<usize>,
    particle_periodic_jumps: Vec<[isize; 2]>,
    number_of_true_particles: usize,
    msd_particles_startpoint: Vec<Particle>,
    lattice_elements: Vec<LatticeElement>,
    buhnen_elements: Vec<SingleBuhne>,
    center_points: Vec<AreaCenter>,
    size: Vector2<f64>,
    pub num_steps: usize,
    time: f64,
    particle_radius: f64,
    config: ModelSpecification,
    geometry_lookup_table: Vec<Vec<usize>>, //SmallVec<[usize; 5]>>,
    particle_lookup_table: Vec<Vec<usize>>,
    rng: Xoshiro256PlusPlus,
    pub rep_num: Option<usize>,
    pub folder_name: String,
}

impl SimulationSystem {
    pub fn execute(&mut self, print_progress: bool) {
        if self.config.sim_param.number_of_replications == 1 {
            match self.config.geometry_param.barrier_type {
                BarrierType::PotentialSum | BarrierType::PotentialNearest => {
                    self.write_sample_potential_files()
                }
                _ => {}
            }
        }
        self.run_until(
            self.config.sim_param.draw_images,
            match self.config.sim_param.write_traj {
                Some(x) => x,
                _ => false,
            },
            print_progress,
        );
    }

    fn write_sample_potential_files(&self) {
        let samples = 50;
        //iterate values

        let force_x: Vec<f64>;
        let force_y: Vec<f64>;

        let sample_particles = itertools_num::linspace::<f64>(0., self.size[0], samples)
            .cartesian_product(itertools_num::linspace::<f64>(0., self.size[1], samples))
            .map(|(x, y)| Particle {
                position: point![x, y],
            });
        let force: Vec<_> = sample_particles
            .map(|p| match self.config.geometry_param.barrier_type {
                BarrierType::PotentialSum => self.compute_lattice_forces(&p),
                BarrierType::PotentialNearest => self.compute_lattice_forces_nearest_only(&p),
                _ => unreachable!(),
            })
            .collect();

        force_x = force.iter().map(|x| x[0]).collect();
        force_y = force.iter().map(|x| x[1]).collect();

        //println!("{:?}", force);

        assert_eq!(force_x.len(), force.len());
        assert_eq!(force_x.len(), samples * samples);
        println!("len is {}", force_x.len());
        println!("{}/force_x.npy", self.folder_name);

        let force_x = Array2::from_shape_vec((samples, samples), force_x).unwrap();
        write_npy(format!("{}/force_x.npy", self.folder_name), &force_x).unwrap();

        let force_y = Array2::from_shape_vec((samples, samples), force_y).unwrap();
        write_npy(format!("{}/force_y.npy", self.folder_name), &force_y).unwrap();
    }

    pub fn new_random(config: ModelSpecification) -> SimulationSystem {
        let particle_radius = config.model_param.particle_radius.value;
        let mut rng = Xoshiro256PlusPlus::from_entropy();
        let mut particles = vec![];
        let size = vector![
            config.geometry_param.system_size[0].value,
            config.geometry_param.system_size[1].value
        ];
        assert!(
            particle_radius * 5. < size[0],
            "particles are to large for system size"
        );
        let mut size_decrease = 1.;
        let mut counter = 0;
        while particles.len() < config.model_param.number_of_particles {
            let position = vector![rng.gen_range(0.0..1.0), rng.gen_range(0.0..1.0)]
                .component_mul(&(size - 2. * vector![1., 1.] * particle_radius))
                + vector![1., 1.] * particle_radius;
            let new_particle = Particle {
                position: Point2::from(position),
            };
            //println!("{:?} vs {:?}", new_particle.position, size);
            assert!(new_particle.position[0] + particle_radius < size[0]);
            assert!(new_particle.position[1] + particle_radius < size[1]);
            assert!(new_particle.position[0] > particle_radius);
            assert!(new_particle.position[1] > particle_radius);
            size_decrease *= 1.0 - 1e-7;
            counter += 1;
            if counter % config.model_param.number_of_particles == 0 && counter > 100 {
                println!(
                    "attempt {}/{} @ {}, done {}",
                    counter,
                    config.model_param.number_of_particles,
                    size_decrease,
                    particles.len() as f64 / config.model_param.number_of_particles as f64
                );
            }
            if particles.iter().all(|other_p| {
                !new_particle.overlapps_fast(other_p, particle_radius * size_decrease)
            }) {
                particles.push(new_particle)
            }
        }
        let mut res = SimulationSystem {
            number_of_true_particles: particles.len(),
            particle_periodic_jumps: vec![[0; 2]; particles.len()],
            particles: particles,
            msd_particles_startpoint: vec![],
            lattice_elements: vec![],
            buhnen_elements: vec![],
            center_points: vec![],
            size: size,
            num_steps: 0,
            time: 0.0,
            particle_radius: particle_radius,
            config: config,
            geometry_lookup_table: vec![],
            particle_lookup_table: vec![vec![]; LOOKUP_N * LOOKUP_N],
            rng,
            rep_num: None,
            folder_name: "".to_string(),
            particle_associated_area: vec![],
        };
        res.add_geometry_file(res.config.geometry_param.name_of_input_file.clone());
        res
    }
    fn add_geometry_file(&mut self, path: String) {
        match self.config.geometry_param.barrier_type {
            BarrierType::NoBarrier => return,
            _ => {}
        }
        let file = File::open(path).expect("could not open file");
        let mut rdr = csv::ReaderBuilder::new()
            .has_headers(false)
            .from_reader(file);
        match self.config.geometry_param.barrier_type {
            BarrierType::Buhnen => {
                self.buhnen_elements = rdr
                    .records()
                    .into_iter()
                    .map(|r| r.unwrap())
                    .map(|r| SingleBuhne {
                        position: point![
                            r.get(0).unwrap().parse().unwrap(),
                            r.get(1).unwrap().parse().unwrap()
                        ] * self.config.geometry_param.input_file_prefactor.value,
                    })
                    .collect();
                self.generate_buhnen_lookup_table();
            }
            BarrierType::PotentialSum | BarrierType::PotentialNearest => {
                self.lattice_elements = rdr
                    .records()
                    .into_iter()
                    .map(|r| {
                        let r = r.unwrap();
                        //println!("{:?}", r);
                        let start_point = vector![
                            r.get(0).unwrap().parse().unwrap(),
                            r.get(1).unwrap().parse().unwrap()
                        ];
                        let end_point = vector![
                            r.get(2).unwrap().parse().unwrap(),
                            r.get(3).unwrap().parse().unwrap()
                        ];
                        /*assert!(start_point <= self.size * 1.1);
                        assert!(end_point <= self.size * 1.1);
                        assert!(start_point >= self.size * (-0.1));
                        assert!(end_point >= self.size * (-0.1));*/
                        LatticeElement {
                            start_point: Point2::from(start_point)
                                * self.config.geometry_param.input_file_prefactor.value,
                            end_point: Point2::from(end_point)
                                * self.config.geometry_param.input_file_prefactor.value,
                        }
                    })
                    .collect();
                self.generate_lattice_lookup_table();
            }
            BarrierType::HoppProbability => {
                self.center_points = rdr
                    .records()
                    .into_iter()
                    .map(|r| r.unwrap())
                    .map(|r| AreaCenter {
                        position: point![
                            r.get(0).unwrap().parse().unwrap(),
                            r.get(1).unwrap().parse().unwrap()
                        ] * self.config.geometry_param.input_file_prefactor.value,
                        id: r.get(2).unwrap().parse().unwrap(),
                    })
                    .collect();
                self.generate_center_point_table();
                self.particle_associated_area = self.compute_associated_areas_for_particles();
            }
            BarrierType::NoBarrier => {
                unreachable!()
            }
        }
    }
    fn x_to_lookup(&self, v: f64) -> usize {
        if v <= 0. {
            0
        } else if v >= self.size[0] {
            LOOKUP_N - 1
        } else {
            //println!("{} * {}", v, self.size[0] / LOOKUP_N as f64);
            (v * LOOKUP_N as f64 / self.size[0]).floor() as usize
        }
    }
    fn y_to_lookup(&self, v: f64) -> usize {
        if v <= 0. {
            0
        } else if v >= self.size[1] {
            LOOKUP_N - 1
        } else {
            (v * LOOKUP_N as f64 / self.size[1]).floor() as usize
        }
    }
    fn xy_to_lookup_cell_idx(&self, point: &Point2<f64>) -> usize {
        let x = self.x_to_lookup(point[0]);
        let y = self.y_to_lookup(point[1]);
        x * LOOKUP_N + y
    }
    fn update_particle_lookp_table(&mut self) {
        if self.particles.len() < SMALL_PARTICLE_LIMIT {
            return;
        }
        for (n, p) in self.particles.iter().enumerate() {
            let idx = self.xy_to_lookup_cell_idx(&p.position);
            self.particle_lookup_table[idx].push(n);
        }
    }
    fn clear_particle_lookup_table_dumm(&mut self) {
        for y in self.particle_lookup_table.iter_mut() {
            y.clear();
        }
    }
    fn clear_particle_lookup_based_on_positions(&mut self) {
        if self.particles.len() < SMALL_PARTICLE_LIMIT {
            return;
        }
        for p in self.particles.iter() {
            let idx = self.xy_to_lookup_cell_idx(&p.position);
            self.particle_lookup_table[idx].clear();
        }
    }
    fn generate_center_point_table(&mut self) {
        assert!(self.geometry_lookup_table.len() == 0);
        self.geometry_lookup_table = vec![Vec::new(); LOOKUP_N * LOOKUP_N];
        for x_i in 0..LOOKUP_N {
            for y_i in 0..LOOKUP_N {
                let probe = Particle {
                    position: point![
                        (x_i as f64 + 0.5) * self.size[0] / LOOKUP_N as f64,
                        (y_i as f64 + 0.5) * self.size[1] / LOOKUP_N as f64
                    ],
                };
                let distances: Vec<f64> = self
                    .center_points
                    .iter()
                    .map(|p| p.distance(&probe))
                    .collect();
                let min_dist = distances
                    .iter()
                    .fold(self.size[0] + self.size[1], |min, &val| {
                        if val < min {
                            val
                        } else {
                            min
                        }
                    });
                let idx = self.xy_to_lookup_cell_idx(&probe.position);
                self.geometry_lookup_table[idx] = distances
                    .into_iter()
                    .enumerate()
                    .filter(|(_, k)| {
                        // todo make constraint harder
                        *k < min_dist + 2. * (self.size[0] + self.size[1]) / LOOKUP_N as f64
                    })
                    .map(|(n, _)| n)
                    .collect();
                assert!(
                    !self.geometry_lookup_table[self.xy_to_lookup_cell_idx(&probe.position)]
                        .is_empty()
                );
                /*println!(
                    "{} {} ({})-> {:?}",
                    x_i,
                    y_i,
                    idx,
                    self.geometry_lookup_table[self.xy_to_lookup_cell_idx(&probe.position)]
                );*/
            }
        }
        self.geometry_lookup_table
            .iter()
            .enumerate()
            .for_each(|(_n, k)| {
                //println!("{}", n);
                assert!(!k.is_empty());
            });
    }

    fn generate_buhnen_lookup_table(&mut self) {
        assert!(self.geometry_lookup_table.len() == 0);
        self.geometry_lookup_table = vec![Vec::new(); LOOKUP_N * LOOKUP_N];
        for (n, lat) in self.buhnen_elements.iter().enumerate() {
            let x_min = self.x_to_lookup(lat.min_extension(&self.config)[0] - self.particle_radius);
            let y_min = self.y_to_lookup(lat.min_extension(&self.config)[1] - self.particle_radius);
            let x_max = self.x_to_lookup(lat.max_extension(&self.config)[0] + self.particle_radius);
            let y_max = self.y_to_lookup(lat.max_extension(&self.config)[1] + self.particle_radius);
            //println!("{} {} -- {} {}", x_min, x_max, y_min, y_max);
            for x in x_min..=x_max {
                for y in y_min..=y_max {
                    self.geometry_lookup_table[x * LOOKUP_N + y].push(n);
                }
            }
        }
    }
    fn generate_lattice_lookup_table(&mut self) {
        assert!(self.geometry_lookup_table.len() == 0);
        self.geometry_lookup_table = vec![Vec::new(); LOOKUP_N * LOOKUP_N];
        for (n, lat) in self.lattice_elements.iter().enumerate() {
            let x_min = self.x_to_lookup(lat.min_extension(&self.config)[0] - self.particle_radius);
            let y_min = self.y_to_lookup(lat.min_extension(&self.config)[1] - self.particle_radius);
            let x_max = self.x_to_lookup(lat.max_extension(&self.config)[0] + self.particle_radius);
            let y_max = self.y_to_lookup(lat.max_extension(&self.config)[1] + self.particle_radius);
            //println!("{} {} -- {} {}", x_min, x_max, y_min, y_max);
            for x in x_min..=x_max {
                for y in y_min..=y_max {
                    self.geometry_lookup_table[x * LOOKUP_N + y].push(n);
                }
            }
        }
    }

    fn boundary_force(&self, particle: &Particle, config: &ModelSpecification) -> Vector2<f64> {
        let mut result_force = vector![0., 0.];
        for d in 0..=1 {
            if self.config.geometry_param.periodic_boundary[d] {
                continue;
            }
            if particle.position[d] < self.particle_radius {
                let overlapp = -(particle.position[d] - self.particle_radius);
                result_force[d] = config
                    .force_magnitude_of_boundary_on_particle(Quantity {
                        dimension: PhantomData,
                        units: PhantomData,
                        value: overlapp,
                    })
                    .value;
            } else if particle.position[d] + self.particle_radius > self.size[d] {
                let overlapp = particle.position[d] + self.particle_radius - self.size[d];
                result_force[d] = -config
                    .force_magnitude_of_boundary_on_particle(Quantity {
                        dimension: PhantomData,
                        units: PhantomData,
                        value: overlapp,
                    })
                    .value;
            }
        }
        result_force
    }
    fn _compute_lattice_forces_slow(&self, particle: &Particle) -> Vector2<f64> {
        self.lattice_elements
            .iter()
            .filter_map(|lattice| lattice.lattice_force_and_distance(particle, &self.config))
            .map(|k| k.0)
            .sum()
    }
    fn compute_buhnen_forces(&self, particle: &Particle) -> Vector2<f64> {
        if self.buhnen_elements.is_empty() {
            return vector![0., 0.];
        }
        self.geometry_lookup_table[self.xy_to_lookup_cell_idx(&particle.position)]
            .iter()
            .map(|idx| &self.buhnen_elements[*idx])
            .filter_map(|buhne| buhne.buhne_force(particle, &self.config))
            .sum()
    }
    fn compute_lattice_forces_nearest_only(&self, particle: &Particle) -> Vector2<f64> {
        if self.lattice_elements.is_empty() {
            return vector![0., 0.];
        }
        let cached_forces: Vec<_> = self.geometry_lookup_table
            [self.xy_to_lookup_cell_idx(&particle.position)]
        .iter()
        .map(|idx| &self.lattice_elements[*idx])
        .filter_map(|lattice| lattice.lattice_force_and_distance(particle, &self.config))
        .collect();

        if cached_forces.is_empty() {
            return vector![0., 0.];
        }

        let direction: Vector2<f64> = cached_forces
            .iter()
            .inspect(|(_, dist)| assert!(*dist > 0.))
            .map(|(dir, dist)| dir.normalize() / dist.powi(2))
            .sum();

        let force_mag: Vector2<_> = cached_forces
            .into_iter()
            .fold(
                (vector![0., 0.], 1e100),
                |a, b| {
                    if a.1 < b.1 {
                        a
                    } else {
                        b
                    }
                },
            )
            .0;
        direction.normalize() * force_mag.norm()
    }
    fn compute_lattice_forces(&self, particle: &Particle) -> Vector2<f64> {
        if self.lattice_elements.is_empty() {
            return vector![0., 0.];
        }
        let check = false;
        if check {
            let lookup_set: HashSet<usize> = self.geometry_lookup_table
                [self.xy_to_lookup_cell_idx(&particle.position)]
            .iter()
            .cloned()
            .collect();
            /*println!(
                "setsize {} of {} vs {}",
                lookup_set.len(),
                self.lattice_elements.len(),
                self.lattice_elements
                    .iter()
                    .filter(|lattice| lattice.lattice_force(particle, &self.config).is_some())
                    .count()
            );*/
            self.lattice_elements
                .iter()
                .enumerate()
                .filter(|(_, lattice)| {
                    lattice
                        .lattice_force_and_distance(particle, &self.config)
                        .is_some()
                })
                .map(|(n, _)| n)
                .for_each(|n| {
                    if !lookup_set.contains(&n) {
                        println!(
                            "missing {}/{} @ {}",
                            self.lattice_elements[n].start_point * 1e9,
                            self.lattice_elements[n].end_point * 1e9,
                            particle.position * 1e9
                        );
                        println!(
                            "min {}",
                            self.xy_to_lookup_cell_idx(
                                &self.lattice_elements[n].min_extension(&self.config)
                            )
                        );
                        println!(
                            "max {}",
                            self.xy_to_lookup_cell_idx(
                                &self.lattice_elements[n].max_extension(&self.config)
                            )
                        );
                        panic!(
                            "lookup to {}",
                            self.xy_to_lookup_cell_idx(&particle.position)
                        )
                    }
                });
            //assert!(lookup_set.contains(&n)));
        }
        self.geometry_lookup_table[self.xy_to_lookup_cell_idx(&particle.position)]
            .iter()
            .map(|idx| &self.lattice_elements[*idx])
            .filter_map(|lattice| lattice.lattice_force_and_distance(particle, &self.config))
            .map(|k| k.0)
            .sum()
    }
    fn _compute_force_between_particles_slow(&self, particle: &Particle, n: usize) -> Vector2<f64> {
        self.particles
            .iter()
            .enumerate()
            .filter(|(nother, _)| *nother != n)
            .filter_map(|(_, other_p)| {
                self.force_between_two_particles(&particle, &other_p, self.particle_radius)
            })
            .sum()
    }

    fn compute_force_between_particles(
        &self,
        particle: &Particle,
        particle_n: usize,
    ) -> Vector2<f64> {
        if self.particles.len() < SMALL_PARTICLE_LIMIT {
            return self
                .particles
                .iter()
                .enumerate()
                .filter(|(nother, _)| *nother != particle_n)
                .filter_map(|(_, other_p)| {
                    self.force_between_two_particles(&particle, &other_p, self.particle_radius)
                })
                .sum();
        }

        let check = false; // for debug

        let x_min = self.x_to_lookup(particle.position[0] - 2. * self.particle_radius);
        let y_min = self.y_to_lookup(particle.position[1] - 2. * self.particle_radius);
        let x_max = self.x_to_lookup(particle.position[0] + 2. * self.particle_radius);
        let y_max = self.y_to_lookup(particle.position[1] + 2. * self.particle_radius);
        //println!("{} {} -- {} {}", x_min, x_max, y_min, y_max);

        if check {
            assert_eq!(
                (x_min..=x_max)
                    .cartesian_product(y_min..=y_max)
                    .map(|(x, y)| self.particle_lookup_table[x * LOOKUP_N + y].iter())
                    .flatten()
                    .filter(|k| **k != particle_n)
                    .map(|k| &self.particles[*k])
                    .filter(|other_p| {
                        self.force_between_two_particles(&particle, other_p, self.particle_radius)
                            .is_some()
                    })
                    .count(),
                self.particles
                    .iter()
                    .enumerate()
                    .filter(|(nother, _)| *nother != particle_n)
                    .filter(|(_, other_p)| {
                        self.force_between_two_particles(&particle, &other_p, self.particle_radius)
                            .is_some()
                    })
                    .count()
            )
        }
        // cheap lookup optimization
        if x_min == x_max && y_min == y_max {
            return self.particle_lookup_table[x_min * LOOKUP_N + y_min]
                .iter()
                .filter(|k| **k != particle_n)
                .map(|k| &self.particles[*k])
                .filter_map(|other_p| {
                    self.force_between_two_particles(&particle, other_p, self.particle_radius)
                })
                .sum();
        }

        //println!("{}-{} and {}-{}", x_min, x_max, y_min, y_max);
        (x_min..=x_max)
            .cartesian_product(y_min..=y_max)
            .map(|(x, y)| self.particle_lookup_table[x * LOOKUP_N + y].iter())
            .flatten()
            .filter(|k| **k != particle_n)
            .map(|k| &self.particles[*k])
            .filter_map(|other_p| {
                self.force_between_two_particles(&particle, other_p, self.particle_radius)
            })
            .sum()
    }
    fn compute_associated_area_for_particle(&self, p: &Particle) -> usize {
        let idx = self.xy_to_lookup_cell_idx(&p.position);
        let (nmin, _) = self.geometry_lookup_table[idx]
            .iter()
            .map(|idx_of_center| {
                (
                    self.center_points[*idx_of_center].id,
                    self.center_points[*idx_of_center].distance_sqrt(&p),
                )
            })
            .fold(
                (usize::MAX, self.size[0] + self.size[1]),
                |(nmin, min), (nval, val)| {
                    if val < min {
                        (nval, val)
                    } else {
                        (nmin, min)
                    }
                },
            );
        nmin
    }

    fn compute_associated_areas_for_particles(&self) -> Vec<usize> {
        self.particles
            .iter()
            .take(self.number_of_true_particles)
            .map(|p| self.compute_associated_area_for_particle(p))
            .collect()
    }

    pub fn step(&mut self, timestep: f64) {
        self.num_steps += 1;

        let diffusion_prefactor: Length =
            (2. * self.config.model_param.diffusion * self.config.sim_param.timestep).sqrt();
        let drift_term = match self.config.model_param.field_frequency {
            None => 1.,
            Some(f) => {
                let time = Time::new::<si::time::second>(self.time);
                let phase: Ratio = (f * time) * std::f64::consts::PI;
                phase.value.sin()
            }
        } * vector![
            self.config.model_param.global_drift[0].value,
            self.config.model_param.global_drift[1].value
        ] * timestep;

        // test units of force dappening
        let _latt_displace: Length = Force::new::<si::force::newton>(1.)
            * self.config.sim_param.timestep
            / self.config.total_friction;

        self.update_particle_lookp_table();

        let random_vectors: Vec<Vector2<f64>> = (0..self.number_of_true_particles)
            .map(|_| {
                //let gausfac: f64 = self.rng.sample(StandardNormal);
                // gausfac * generate_random_normalized_vector(&mut self.rng)  // Philipp: You don't have to multiply gausfac to the (now not normalized) random vector here
                generate_random_normalized_vector(&mut self.rng)
            })
            .collect();

        let steps: Vec<Vector2<f64>> = self
            .particles
            .iter()
            .take(self.number_of_true_particles)
            .enumerate()
            .zip(random_vectors.into_iter())
            .map(|((n, particle), random_vec)| {
                //let mut hopp_multiplier = 1.;
                let diffusion: Vector2<f64> = diffusion_prefactor.value * random_vec;
                let forces_between_particles = self.compute_force_between_particles(&particle, n);
                let geometry_force: Vector2<f64> = match self.config.geometry_param.barrier_type {
                    BarrierType::Buhnen => self.compute_buhnen_forces(&particle),
                    BarrierType::PotentialSum => self.compute_lattice_forces(&particle),
                    BarrierType::PotentialNearest => {
                        self.compute_lattice_forces_nearest_only(&particle)
                    }
                    BarrierType::HoppProbability => {
                        vector![0., 0.]
                        //nothing to do
                    }
                    BarrierType::NoBarrier => {
                        vector![0., 0.]
                    }
                };
                //let lattice_forces: Vector2<f64> = self._compute_lattice_forces_slow(&particle);
                /*println!(
                    "{} {} {} {}",
                    diffusion.norm() / drift_term.norm(),
                    forces_between_particles.norm() / drift_term.norm(),
                    lattice_forces.norm() / drift_term.norm(),
                    self.boundary_force(particle, &self.config).norm() / drift_term.norm()
                );*/
                drift_term
                    + diffusion
                    + (forces_between_particles
                    + geometry_force //* self.config.force_attinuation.value
                    + self.boundary_force(particle, &self.config))
                        / self.config.total_friction.value
                        * timestep
            })
            .collect();
        /*match self.config.geometry_param.barrier_type {
            BarrierType::HoppProbability => {
                let areas = self.compute_associated_areas_for_particles_w_step(&steps);
                for ((step, old), new) in steps
                    .iter_mut()
                    .zip(self.particle_associated_area.iter())
                    .zip(areas.iter())
                {
                    println!("{} {}", old, new);
                    if *old != *new {
                        //
                        // if !self.rng.gen_bool(0.001) {
                        //println!("no hop!");
                        *step = vector![0., 0.];
                        //}
                    }
                }
                self.particle_associated_area = areas;
            }
            _ => {}
        }*/
        for k in steps.iter() {
            if k[0] + k[1]
                > self.config.geometry_param.system_size[0].value
                    + self.config.geometry_param.system_size[1].value
            {
                panic!("Numerical error! Step size or force to large. A particle has moved to much in one step.")
            }
        }

        if self.number_of_true_particles > 1000 {
            self.clear_particle_lookup_table_dumm();
        } else {
            self.clear_particle_lookup_based_on_positions();
        }
        self.particles
            .resize_with(self.number_of_true_particles, || panic!());
        assert_eq!(self.particles.len(), self.number_of_true_particles);
        assert_eq!(
            self.particle_periodic_jumps.len(),
            self.number_of_true_particles
        );
        let use_hopping = match self.config.geometry_param.barrier_type {
            BarrierType::HoppProbability => true,
            _ => false,
        };

        let mut peridoic_jumps_tmp = vec![];
        swap(&mut peridoic_jumps_tmp, &mut self.particle_periodic_jumps);
        let boundary_size = self.config.geometry_param.system_size;
        let periodec = self.config.geometry_param.periodic_boundary;

        let old_positions;
        let old_jump_counts;
        if use_hopping {
            old_positions = self.particles.clone();
            old_jump_counts = peridoic_jumps_tmp.clone();
        } else {
            old_positions = vec![];
            old_jump_counts = vec![];
        }
        self.particles
            .iter_mut()
            .zip(steps.into_iter())
            .zip(peridoic_jumps_tmp.iter_mut())
            .for_each(|((particle, step), jump)| {
                particle.position += step;
                for idx in 0..=1 {
                    if periodec[idx] {
                        if particle.position[idx] < 0. {
                            particle.position[idx] += boundary_size[idx].value;
                            jump[idx] -= 1;
                        }
                        if particle.position[idx] >= boundary_size[idx].value {
                            particle.position[idx] -= boundary_size[idx].value;
                            jump[idx] += 1;
                        }
                    }
                }
            });
        swap(&mut peridoic_jumps_tmp, &mut self.particle_periodic_jumps);
        if use_hopping {
            // reset movement if hoppend
            let mut old_areas = vec![];
            let mut new_areas = self.compute_associated_areas_for_particles();
            let phopp = self
                .config
                .model_param
                .hopp_probability
                .expect("Hopping probability not set in input file");
            assert!(phopp >= 0.);
            assert!(phopp <= 1.);
            swap(&mut old_areas, &mut self.particle_associated_area);
            for (particle_new, particle_old, jumps_new, jumps_old, new_area, old_area) in izip!(
                self.particles.iter_mut(),
                old_positions.into_iter(),
                self.particle_periodic_jumps.iter_mut(),
                old_jump_counts.into_iter(),
                new_areas.iter_mut(),
                old_areas.iter()
            ) {
                /*println!(
                    " {} {} {:?}/{:?} ",
                    new_area,
                    old_area,
                    particle_new.position * 1e9,
                    particle_old.position * 1e9
                );*/

                if *new_area != *old_area {
                    if !self.rng.gen_bool(phopp) {
                        *new_area = *old_area;
                        //    println!("{:?} {} {}", particle_new, new_areas, old_areas);

                        // do probability check
                        *jumps_new = jumps_old;
                        *particle_new = particle_old;
                        /* assert_eq!(
                            self.compute_associated_area_for_particle(&particle_new),
                            old_areas
                        );*/
                        //*jumps_new = jumps_old;/
                    }
                }
            }
            self.particle_associated_area = new_areas;
        }
        /*assert_eq!(
            self.particle_associated_area[0],
            self.compute_associated_area_for_particle(&self.particles[0])
        );
        assert_eq!(self.particle_associated_area[0], old_areas[0]);*/

        let extended: Vec<_> = self
            .particles
            .iter()
            .map(|particle| {
                let mut result = vec![];
                let mut x_was_periodic = 0;
                for idx in 0..=1 {
                    if self.config.geometry_param.periodic_boundary[idx] {
                        if particle.position[idx] - self.particle_radius < 0. {
                            let mut delta = vector![0., 0.];
                            delta[idx] = self.config.geometry_param.system_size[idx].value;
                            result.push(Particle {
                                position: particle.position.clone() + delta,
                            });
                            if idx == 0 {
                                x_was_periodic = 1;
                            } else if x_was_periodic != 0 {
                                delta[0] = self.config.geometry_param.system_size[idx].value
                                    * x_was_periodic as f64;
                                result.push(Particle {
                                    position: particle.position.clone() + delta,
                                });
                            }
                        }
                        if particle.position[idx] + self.particle_radius
                            > self.config.geometry_param.system_size[idx].value
                        {
                            let mut delta = vector![0., 0.];
                            delta[idx] = -self.config.geometry_param.system_size[idx].value;
                            result.push(Particle {
                                position: particle.position.clone() + delta,
                            });
                            if idx == 0 {
                                assert_eq!(x_was_periodic, 0);
                                x_was_periodic = -1;
                            } else if x_was_periodic != 0 {
                                delta[0] = self.config.geometry_param.system_size[idx].value
                                    * x_was_periodic as f64;
                                result.push(Particle {
                                    position: particle.position.clone() + delta,
                                });
                            }
                        }
                    }
                }
                result
            })
            .collect();
        self.particles.extend(extended.into_iter().flatten());
        self.time += timestep;
    }

    fn force_between_two_particles(
        &self,
        particle_a: &Particle,
        particle_b: &Particle,
        radius: f64,
    ) -> Option<Vector2<f64>> {
        if !particle_a.overlapps_fast(particle_b, radius) {
            None
        } else {
            let distance = particle_a.distance(particle_b, radius);
            Some(
                self.config
                    .force_magnitude_between_particles(Quantity {
                        dimension: PhantomData,
                        units: PhantomData,
                        value: -distance,
                    })
                    .value
                    * (&particle_a.position - &particle_b.position).normalize(),
            )
        }
    }

    pub fn run_until(
        &mut self,
        //num_observations: usize,
        draw_images: bool,
        write_trajectory_files: bool,
        give_individual_progress: bool,
    ) {
        let mut trajectories: Vec<Vec<OutputRow>> = vec![];
        let mut start_writing_msd = false;
        let mut corrected_positions: Vec<(f64, Vec<Vector2<f64>>)> = vec![];
        let integration_time_step = self.config.sim_param.timestep.value;
        let num_observations = (self.config.sim_param.simulation_time.value
            / (integration_time_step * self.config.sim_param.observe_every_steps as f64))
            as usize;
        /*assert!(
            integration_time_step <= target_time / num_observations as f64,
            "timestep larger than observation"
        );*/
        let now = Instant::now();
        let expected_num_steps =
            (self.config.sim_param.simulation_time.value / integration_time_step) as usize;
        let pb = ProgressBar::new(expected_num_steps as u64);
        let pb_opti_fact = (num_observations / 100000).max(1);
        pb.set_style(ProgressStyle::default_bar()
            .template("{spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {per_sec} ({eta})")
            //.with_key("eta", |state| format!("{:.1}s", state.eta().as_secs_f64()))
            .progress_chars("#>-"));
        let mut msd_writing_truly_started_at: f64 = f64::NEG_INFINITY;
        for n in 0..num_observations {
            for _ in 0..self.config.sim_param.observe_every_steps {
                self.step(integration_time_step);
                if !start_writing_msd {
                    match self.config.sim_param.start_msd_time {
                        Some(x) => {
                            if x.value <= self.time && self.msd_particles_startpoint.is_empty() {
                                self.particle_periodic_jumps =
                                    vec![[0; 2]; self.number_of_true_particles];
                                self.msd_particles_startpoint = self
                                    .particles
                                    .iter()
                                    .take(self.number_of_true_particles)
                                    .cloned()
                                    //overwrite with random
                                    /*.map(|_| Particle {
                                        position: point![
                                            thread_rng().gen_range(
                                                0.0...
                                                self.config.geometry_param.system_size[0].value
                                            ),
                                            thread_rng().gen_range(
                                                0.0...
                                                self.config.geometry_param.system_size[1].value
                                            )
                                        ],
                                    })*/
                                    .collect();
                                start_writing_msd = true;
                            }
                        }
                        None => {}
                    }
                }
            }
            if start_writing_msd {
                assert_eq!(
                    self.number_of_true_particles,
                    self.particle_periodic_jumps.len()
                );
                if corrected_positions.is_empty() {
                    msd_writing_truly_started_at = self.time;
                }
                corrected_positions.push((
                    self.time - msd_writing_truly_started_at,
                    self.particles
                        .iter()
                        .take(self.number_of_true_particles)
                        .zip(self.msd_particles_startpoint.iter())
                        .zip(self.particle_periodic_jumps.iter())
                        .map(|((total_start, b), jumps)| {
                            total_start.position - b.position
                                + vector![
                                    jumps[0] as f64
                                        * self.config.geometry_param.system_size[0].value,
                                    jumps[1] as f64
                                        * self.config.geometry_param.system_size[1].value
                                ]
                        })
                        .collect(),
                ))
            }
            if n % pb_opti_fact == 0 && give_individual_progress {
                pb.set_position(self.num_steps as u64);
            }
            if write_trajectory_files {
                trajectories.push(
                    self.particles
                        .iter()
                        .take(self.number_of_true_particles)
                        .map(|p| OutputRow {
                            // 1e9 because of nanosecond
                            time: self.time as f32 * 1e9,
                            x: p.position[0] as f32 * 1e9,
                            y: p.position[1] as f32 * 1e9,
                        })
                        .collect(),
                );
            }
            if draw_images {
                self.write_image(format!("{}/image_{:05}.png", self.folder_name, n,));
            }
        }
        if give_individual_progress {
            pb.finish_and_clear();

            println!(
                "{} steps in {}s ({} steps per second)",
                self.num_steps,
                now.elapsed().as_secs_f64(),
                self.num_steps as f64 / now.elapsed().as_secs_f64()
            );
        }
        let post_proccess_time = Instant::now();
        let rep_string = match self.rep_num {
            None => "".to_string(),
            Some(y) => {
                format!("r{}_", y)
            }
        };
        if !corrected_positions.is_empty() {
            let time_points: Vec<f64> = corrected_positions
                .iter()
                .map(|(t, _)| *t)
                .take(corrected_positions.len() / 8 - 1)
                .collect();
            let msd: Vec<f64> = (0..time_points.len())
                .map(|n| {
                    let my_msd: Vec<f64> = corrected_positions
                        .iter()
                        .map(|(_time, positions)| positions)
                        .step_by(n + 1)
                        .tuple_windows()
                        .map(|(old, new)| {
                            assert_eq!(new.len(), self.number_of_true_particles);
                            assert_eq!(new.len(), old.len());
                            old.iter()
                                .zip(new.iter())
                                .map(|(o, n)| (o - n).norm_squared() as f64)
                                .sum::<f64>()
                                / self.number_of_true_particles as f64
                        })
                        .collect();
                    ////return my_msd[0];
                    return my_msd.iter().sum::<f64>() / my_msd.len() as f64;
                    /*.sum::<f64>()
                    / (slices.count() as f64 / 2.).floor()*/
                })
                .collect();
            //println!("{:?}", msd);
            assert!(msd[0] > 0.);
            assert_eq!(time_points[0], 0.);
            let arr = Array2::from_shape_vec(
                (msd.len(), 2),
                vec![0.0]
                    .into_iter()
                    .chain(msd.into_iter())
                    .zip(time_points.into_iter())
                    .map(|(msd, time)| vec![time, msd].into_iter())
                    .flatten()
                    .collect(),
            )
            .unwrap();
            write_npy(format!("{}/msd_{}.npy", self.folder_name, rep_string), &arr).unwrap();
        }

        if write_trajectory_files {
            let trajectories = transpose2(trajectories);
            //println!("writing files");

            let write_csv = false;
            for (n, traj) in trajectories.into_iter().enumerate() {
                if write_csv {
                    let mut wtr = csv::Writer::from_path(format!(
                        "{}/trajectory_{}p{}.csv",
                        self.folder_name, rep_string, n
                    ))
                    .unwrap();
                    for (_nt, dat) in traj.iter().enumerate() {
                        wtr.serialize(dat).unwrap();
                    }
                } /*else*/
                {
                    let length = traj.len();
                    let converted: Vec<f32> = traj
                        .into_iter()
                        .map(|k| vec![k.time, k.x, k.y].into_iter())
                        .flatten()
                        .collect();
                    let arr = Array2::from_shape_vec((length, 3), converted).unwrap();
                    write_npy(
                        format!("{}/trajectory_{}p{}.npy", self.folder_name, rep_string, n),
                        &arr,
                    )
                    .unwrap();
                }
            }
        }
        if give_individual_progress {
            println!(
                "IO and post proccessing (msd) in {} seconds",
                post_proccess_time.elapsed().as_secs_f64()
            );
        }
    }

    pub fn write_image(&self, path: String) {
        let imsize: u32 = 1080; //* 4;
        let mut image = RgbImage::new(imsize, imsize);

        let white = Rgb([255u8, 255u8, 255u8]);
        draw_filled_rect_mut(&mut image, Rect::at(0, 0).of_size(imsize, imsize), white);
        let scale_factor = imsize as f64 / self.size.max();

        for b in self.buhnen_elements.iter() {
            //println!("{}", s.position);
            //println!("c: {}", color_idx);
            let blue = Rgb([40u8, 255u8, 40u8]);
            draw_filled_circle_mut(
                &mut image,
                (
                    (b.position[0] * scale_factor) as i32,
                    (b.position[1] * scale_factor) as i32,
                ),
                (self.config.geometry_param.buhnen_radius.unwrap().value * scale_factor) as i32,
                blue,
            );
        }
        for p in self.lattice_elements.iter() {
            let red = Rgb([255u8, 50u8, 25u8]);
            //println!(" {:?} {:?}", p.start_point, p.end_point);
            draw_line_segment_mut(
                &mut image,
                (
                    (p.start_point[0] * scale_factor) as f32,
                    (p.start_point[1] * scale_factor) as f32,
                ),
                (
                    (p.end_point[0] * scale_factor) as f32,
                    (p.end_point[1] * scale_factor) as f32,
                ),
                red,
            );
        }
        for (n, s) in self.particles.iter().enumerate() {
            //println!("{}", s.position);
            let color_idx = ((n % self.number_of_true_particles) * 3) % 255;
            //println!("c: {}", color_idx);
            let blue = Rgb([40u8, color_idx as u8, 255u8]);
            draw_filled_circle_mut(
                &mut image,
                (
                    (s.position[0] * scale_factor) as i32,
                    (s.position[1] * scale_factor) as i32,
                ),
                (self.particle_radius * scale_factor) as i32,
                blue,
            );
        }
        //println!("image to {}", path);
        image.save(path).unwrap();
    }
}
