use lazy_static::*;
use serde::{Deserialize, Serialize};
use uom::si;
use uom::si::f64::*;
use uom::si::Quantity;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum BarrierType {
    Buhnen,
    PotentialSum,
    PotentialNearest,
    HoppProbability,
    NoBarrier,
}

// frequently used units here to avoid always putting full path
use uom::si::length::nanometer;
use uom::typenum::{N1, N3, P1, P2, Z0};

type _Viscosity = Quantity<
    uom::si::ISQ<
        N1, /*Length*/
        P1, /*mass*/
        N1, /*Time*/
        Z0, /*current*/
        Z0, /*temp*/
        Z0, /*amount*/
        Z0, /*lum*/
    >,
    uom::si::SI<f64>,
    f64,
>;

type Diffusion = Quantity<
    uom::si::ISQ<
        P2, /*Length*/
        Z0, /*mass*/
        N1, /*Time*/
        Z0, /*current*/
        Z0, /*temp*/
        Z0, /*amount*/
        Z0, /*lum*/
    >,
    uom::si::SI<f64>,
    f64,
>;

type _ElectricFieldStrength = Quantity<
    uom::si::ISQ<
        P1, /*Length*/
        P1, /*mass*/
        N3, /*Time*/
        N1, /*current*/
        Z0, /*temp*/
        Z0, /*amount*/
        Z0, /*lum*/
    >,
    uom::si::SI<f64>,
    f64,
>;

// Put constants that will not change here
lazy_static! {
    static ref BOLTZMANN_CONSTANT: HeatCapacity =
        HeatCapacity::new::<si::heat_capacity::joule_per_kelvin>(1.380649e-23);
    static ref MASS_OF_THE_MOON: Mass = Mass::new::<si::mass::kilogram>(7.341e22);
}

// put here the parameters that are read in for each Simulation setup (i.e. Model parameters)
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ReadInModelParameters {
    pub particle_radius: Length,
    //pub total_friction: MassRate,
    pub lattice_distance_force_cutoff: Length,
    pub temperature: ThermodynamicTemperature,
    pub field_frequency: Option<Frequency>,
    pub k_barr: Energy,
    pub hopp_probability: Option<f64>,
    pub number_of_particles: usize,
    pub diffusion: Diffusion,
    pub global_drift: [Velocity; 2],
}

// Geometry Parameters
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ReadInGeometryParameters {
    pub system_size: [Length; 2],
    pub name_of_input_file: String,
    pub input_file_prefactor: Length, // unit of lattice file (probably nanometer)
    pub periodic_boundary: [bool; 2],
    pub buhnen_radius: Option<Length>,
    pub barrier_type: BarrierType,
}

// put here the parameters that define the execution of each Model
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ReadInSimulationParameters {
    pub number_of_replications: usize,
    pub timestep: Time,
    pub draw_images: bool,
    pub output_prefix: String,
    pub simulation_time: Time,
    pub observe_every_steps: usize,
    pub start_msd_time: Option<Time>,
    pub write_traj: Option<bool>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct InputFile {
    pub model_param: ReadInModelParameters,
    pub geometry_param: ReadInGeometryParameters,
    pub sim_param: ReadInSimulationParameters,
}

impl InputFile {
    pub fn generate_sample_file() -> InputFile {
        InputFile {
            model_param: ReadInModelParameters {
                particle_radius: Length::new::<nanometer>(8.),
                lattice_distance_force_cutoff: Length::new::<si::length::nanometer>(20.),
                temperature: ThermodynamicTemperature::new::<si::thermodynamic_temperature::kelvin>(
                    300.,
                ),
                field_frequency: None, //Some(Frequency::new::<si::frequency::hertz>(1.)), //None,
                k_barr: Energy::new::<si::energy::joule>(0.),
                diffusion: Area::new::<si::area::square_micrometer>(0.8)
                    / Time::new::<si::time::second>(1.),
                global_drift: [
                    Velocity::new::<si::velocity::meter_per_second>(0.),
                    Velocity::new::<si::velocity::meter_per_second>(0.),
                ],
                number_of_particles: 10,
                hopp_probability: Some(0.1),
            },
            geometry_param: ReadInGeometryParameters {
                system_size: [
                    Length::new::<nanometer>(300.),
                    Length::new::<nanometer>(300.),
                ],
                name_of_input_file: "mesh_points".to_string(),
                input_file_prefactor: Length::new::<nanometer>(1.),
                periodic_boundary: [true, true],
                buhnen_radius: Some(Length::new::<nanometer>(4.)),
                barrier_type: BarrierType::HoppProbability,
            },
            sim_param: ReadInSimulationParameters {
                number_of_replications: 1,
                timestep: Time::new::<si::time::nanosecond>(200.),
                draw_images: false,
                output_prefix: "smpl".to_string(),
                simulation_time: Time::new::<si::time::second>(20.),
                observe_every_steps: 5000,
                start_msd_time: Some(Time::new::<si::time::second>(1.)),
                write_traj: Some(true),
            },
        }
    }
}

// list parameters
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ModelSpecification {
    pub model_param: ReadInModelParameters,
    pub geometry_param: ReadInGeometryParameters,
    pub sim_param: ReadInSimulationParameters,

    pub total_friction: MassRate,
}

impl ModelSpecification {
    pub fn generate_from_input_file(ipf: InputFile) -> ModelSpecification {
        let boltzmann = HeatCapacity::new::<si::heat_capacity::joule_per_kelvin>(1.380649e-23);

        ModelSpecification {
            model_param: ipf.model_param.clone(),
            geometry_param: ipf.geometry_param.clone(),
            sim_param: ipf.sim_param.clone(),
            total_friction: boltzmann * ipf.model_param.temperature / ipf.model_param.diffusion,
        }
    }
}

impl ModelSpecification {
    pub fn force_magnitude_of_lattice_on_particle(&self, distance: Length) -> Force {
        assert!(
            distance.abs() < self.model_param.lattice_distance_force_cutoff,
            "{:?} < {:?}",
            distance,
            self.model_param.lattice_distance_force_cutoff
        );
        //let sigma = Length::new::<si::length::nanometer>(1.);
        //let exponent: Ratio = -distance * distance / (2. * sigma * sigma);
        //self.model_param.k_barr * distance / (sigma * sigma) * (exponent.value).exp()

		let x0 = Length::new::<si::length::nanometer>(2.5);
        self.model_param.k_barr/x0
    }
    pub fn force_magnitude_between_particles(&self, overlapp: Length) -> Force {
        assert!(overlapp > Length::new::<nanometer>(0.));
        //let r = Length::new::<si::length::nanometer>(8.) * 1e15;
        //let r = self.model_param.particle_radius;
        //let elas: Pressure = Pressure::new::<si::pressure::pascal>(5.0e8);
        let f =
            Force::new::<si::force::newton>(2.5e-11) / Length::new::<si::length::nanometer>(1.);
			//Force::new::<si::force::newton>(2.3154e-11) / Length::new::<si::length::nanometer>(1.);
        //distance is outer distance between particles
        //println!("overlapp: {:?}", overlapp);
        //4. / 3. * elas * (r * overlapp * overlapp * overlapp).sqrt()
        3.0 * f * overlapp
    }

    pub fn force_magnitude_of_boundary_on_particle(&self, overlapp: Length) -> Force {
        assert!(overlapp > Length::new::<nanometer>(0.)); // distance is less than zero -> there is overlapp

        let linear_force_contast = Force::new::<si::force::nanonewton>(0.00366);
        linear_force_contast
    }
}
