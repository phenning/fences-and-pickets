use diffusion_sim::*;
use indicatif::ParallelProgressIterator;
use indicatif::ProgressBar;
use rand::prelude::*;
use rayon::prelude::*;
use std::fs::File;
use std::io::{Read, Write};

fn drop_sample_config() {
    let sample_config = InputFile::generate_sample_file();
    let config_example = toml::to_string(&sample_config).unwrap();
    {
        let mut file = std::fs::File::create("sample_config.toml").unwrap();
        file.write_all(config_example.as_bytes()).unwrap();
    }
}

use clap::{App, Arg};
use rand::distributions::Alphanumeric;

fn main() {
    let matches = App::new("Diffusion simulator")
        //.version("1.0")
        //.author("Kevin K. <kbknapp@gmail.com>")
        //.about("Does awesome things")
        .arg(Arg::with_name("config-files").multiple(true))
        .get_matches();
    let files: Vec<_> = match matches.values_of("config-files") {
        None => {
            println!("You have not specified a simulation configuration. You need to specify at least one. I've written so file 'sample_config.toml' as example.");
            drop_sample_config();
            vec![]
        }
        Some(x) => x.collect(),
    };
    if files.is_empty() {
        return;
    }

    use chrono::prelude::*;
    let utc_stamp = Local::now().to_rfc3339_opts(SecondsFormat::Secs, false) + "_";
    let utc_stamp = utc_stamp.replace(":", "."); //Philipp: Windows don't allow ":" in file names
    let _random_str: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(4)
        .map(char::from)
        .collect();

    let configs: Vec<InputFile> = files
        .into_iter()
        .map(|f| {
            let mut content: String = "".to_string();
            File::open(f)
                .expect("file not found")
                .read_to_string(&mut content)
                .expect("reading file failed");
            toml::from_str(&content).expect("could not parse config file")
        })
        .collect();

    let simulations: Vec<_> = configs
        .into_iter()
        //.into_par_iter()
        .map(|ipf| {
            let ms = ModelSpecification::generate_from_input_file(ipf);
            assert!(ms.sim_param.number_of_replications > 0);
            let mut result = vec![];
            let random_str: String = thread_rng()
                .sample_iter(&Alphanumeric)
                .take(4)
                .map(char::from)
                .collect();
            let prfx =
                utc_stamp.clone() + "_" + &ms.sim_param.output_prefix.clone() + "_" + &random_str;
            println!("test output = {}", prfx);
            std::fs::create_dir(&prfx).expect("could not create folder");
            let mut f = std::fs::File::create(format!("{}/configuration.txt", prfx)).unwrap();
            write!(f, "##{}##\n{:#?}", prfx, ms).unwrap();
            for k in 0..ms.sim_param.number_of_replications {
                result.push((ms.clone(), prfx.clone(), k));
            }
            result.into_iter()
        })
        .flatten()
        .collect();

    if simulations.len() == 1 {
        let mut sim = SimulationSystem::new_random(simulations[0].0.clone());
        sim.folder_name = simulations[0].1.clone();
        sim.execute(true);
    } else {
        let pb = ProgressBar::new(simulations.len() as u64);
        let _total_num_steps: usize = simulations
            .into_par_iter()
            .progress_with(pb)
            .map(|(ipf, prefix, num)| {
                let mut s = SimulationSystem::new_random(ipf);
                s.rep_num = Some(num);
                s.folder_name = prefix;
                s
            })
            .map(|mut s| {
                s.execute(false);
                s.num_steps
            })
            .sum();
    }
    println!("Simulator done");
}
